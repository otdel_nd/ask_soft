// изменил для проверки git
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include </home/user/devel/acs_som_sdk/psp/buildroot-2013.05/output/host/usr/arm-buildroot-linux-gnueabi/sysroot/usr/include/curl/curl.h>
#define PATH_DEV "/dev/ttyS1"
#define PATH_CONFIG "/etc/ask/ask.conf"

int open_dev(char *);			 	// функция, открывающая устройство (возвращает -1, если не удалось открыть)
FILE *fp;					// указатель на файл устройства
FILE *fconfig;					// указатель на файл с конфигурацией АСК
char gps_str[100] = {'0'};			// массив, содержащий GPRMC-строку (от GPS)
char gps_struct(double, double);		// функция для преобразования широты и долготы в градусы, минуты, секунды и помещения в структуру gps_data
void post_send();				// функция для отправки данных на сайт
void get_post_data();				// упаковка пакета post_data для последующей отправки
double get_time();				// функция для получения времени и текущего значения секунд с учетом микросекунд
void read_gps_str();				// функция для чтения строки из GPS-модуля
void clear_gps_str();				// функция очищает массив gps_str
void clear_post_data();				// функция очищает массив post_data
void get_config();				// функция читает данные из файла конфигурации

struct tm *ptime;                               // указатель на структуру типа tm, для хранения времени
time_t lt;		                        // сколько секунд прошло от большого взрыва
double shirota, dolgota;			// числовые широта и долгота

struct  	 				// структура для упаковки широты и долготы в POST-посылку
{
	char flag;				// если flag = 1, то GPS-данные корректны
	int c1;					// градус широты
	int c2;					// минуты широты
	double c3;				// доли минут широты
	int c4;					// градус долготы
	int c5;					// минуты долготы
	double c6;				// доли минут долготы
} gps_data;

struct						// структура c текущими параметрами локомотива
{
	int ln;					// номер локомотива (из файла в PATH_CONFIG)
	char position;				// позиция тяги
	double speed;				// скорость локомотива
	int rpm;
	int a1;
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        double fuel;							// топливо
} loc_data;

char* post_data;							// массив для хранения посылки POST

int main()
{
	double oldt = get_time(), newt = oldt;
	get_config();							// получим данные из файла конфигурации
	{
		do{
		newt = get_time();					// реализация таймера
			if(newt - oldt > 10.)				// для отправки сообщения каждые 60 секунд
			{
				oldt = newt;
			        read_gps_str();                         // прочитаем строку из GPS-модуля
			 	gps_data.flag = gps_struct(shirota, dolgota);                   // преобразуем широту и долготу и поместим в структуру gps_data
				if(gps_data.flag)
				{
					if((post_data = calloc(200, sizeof(char)))==NULL) // выделяем память под хранение POST строки
					{
						printf("Ошибка при распределении памяти\n");
						exit(1);
					}
					get_post_data();		// формируем посылку
					post_send();			// отправляем посылку
					free(post_data);		// освобождаем память
				}
			}
		}while(1);						// выполнять бесконечно
	}
	return 0;
}

int open_dev(char *path_dev)
{
	if ((fp=fopen(path_dev, "a+")) == NULL)
		{
			printf("Не удалось открыть порт.");
			return -1;
		}
		else
		{
			printf("Удалось открыть порт.");
			return 0;
		}
}

char gps_struct(double shi, double dolg)
{
        gps_data.c1 = (int)(shi/100);
        gps_data.c2 = ((int)shi) - gps_data.c1*100;
        gps_data.c3 = shi > 9999 ? (((shi - (int)shi)*100000)*60)/10000 : (((shi - (int)shi)*10000)*60)/10000;

        gps_data.c4 = (int)(dolg/100);
        gps_data.c5 = ((int)dolg) - gps_data.c4*100;
        gps_data.c6 = dolg > 9999 ? (((dolg - (int)dolg)*100000)*60)/10000 : (((dolg - (int)dolg)*10000)*60)/10000;

	if (((gps_data.c1 >= 30 && gps_data.c1 <= 81) &&
	     (gps_data.c4 >= 10 && gps_data.c4 <= 170)))		// проверка правильности координат
	{
		printf("\nGPS-данные корректны: \nШирота = %f\nДолгота = %f\n", shirota, dolgota);
		return 1;
	}
        else
        {
       		printf("\nGPS-данные некорректны: \nШирота = %f\nДолгота = %f\n", shirota, dolgota);
		return 0;
	}
}

void post_send()
{
	printf("Отправляем на сайт...\n");
	CURL *curl;							// дескриптор curl
	CURLcode res;							// перечисление типа CURLcode для хранения кода результата отправки
	curl_global_init(CURL_GLOBAL_ALL);

	curl = curl_easy_init();					// открываем сессию, curl - easy heandle для использования в остальных функциях easy curl

	if(curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL,
				 "http://ask-vnikti.ru/connect.php");	// устанавливаем параметры предстоящей передачи - адрес, куда передаем
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);	// строка для отправки методом POST
		res = curl_easy_perform(curl);				// отправляем данные, результат отправки присваиваем res

	        if(res != CURLE_OK)					// анализируем на предмет отсутствия ошибок
		{
			printf ("Отправка сообщения провалилась, код ошибки: %i,%s\n", res, curl_easy_strerror(res));
		}
		else
		{
			printf("Отправлена строка:\n%s\n", post_data);
		}
		curl_easy_cleanup(curl);				// очищаем сессию
	}
}

void get_post_data()
{
	char tmp[25];							// временный массив
	get_time();							// узнаем время
	strcat(post_data, "dt=");					// начинаем формировать посылку, пакуем:
	strftime(tmp, 25, "%Y-%m-%d %H:%M:%S", ptime);			// время и дату
	strcat(post_data, tmp);

	if(loc_data.ln)							// номер локомотив
	{
		strcat(post_data, "&ln=");
		sprintf(tmp, "%d", loc_data.ln);
		strcat(post_data, tmp);
	}
	if(loc_data.fuel)						// количество топлива (проверку надо другую)
	{
		strcat(post_data, "&fuel=");
		sprintf(tmp, "%d", (int)loc_data.fuel);
		strcat(post_data, tmp);
	}
	if(gps_data.flag)						// координаты GPS, если они правильные
        {
                strcat(post_data, "&c1=");
                sprintf(tmp, "%d", gps_data.c1);
                strcat(post_data, tmp);

		strcat(post_data, "&c2=");
                sprintf(tmp, "%02d", gps_data.c2);
                strcat(post_data, tmp);

		strcat(post_data, "&c3=");
                sprintf(tmp, "%f", gps_data.c3);
                strcat(post_data, tmp);

		strcat(post_data, "&c4=");
                sprintf(tmp, "%d", gps_data.c4);
                strcat(post_data, tmp);

                strcat(post_data, "&c5=");
                sprintf(tmp, "%02d", gps_data.c5);
                strcat(post_data, tmp);

                strcat(post_data, "&c6=");
                sprintf(tmp, "%f", gps_data.c6);
                strcat(post_data, tmp);
        }
}

double get_time()
{
	// для таймера
	double t;							// возвращаемое значение в секундах (с учетом микросекунд)
	struct timeval tv;						// структура для хранения времени
	gettimeofday(&tv, NULL);					// получаем системное время
	t = tv.tv_sec + ((double)tv.tv_usec)/1e6;			// время в секундах (с учетом микросекунд)

	// для формирования посылки
        lt = time(NULL);                    				// сколько секунд прошло от большого взрыва
        ptime = localtime(&lt);                 			// заполняем структуру

	return t;                                                       // возвратим время в секундах
}

void read_gps_str()
{
        char tmp[10]={'0','0','0','0','0','0','0','0','0','0'};		// временный массив для хранения
        int i,j;                               				// строковых широты и долготы

        open_dev(PATH_DEV);                    				// получаем указатель на файл устройства
                                                			// теперь fp это последовательный порт
	clear_gps_str();
        while(strncmp(gps_str, "$GPRMC,", 7))   			// пока gps_str не будет начинаться с "$GPRMC,"
                fgets(gps_str, 100, fp);        			// закидываем в gps_str данные с порта
                                                			// теперь gps_str содержит то, что нужно

        for(i=20, j=0; j<9; i++, j++)           			// выковыриваем широту из
                tmp[j]=gps_str[i];              			// из gps_str
        shirota=atof(tmp);                      			// и преобразуем её в число (double)

        for(i=32, j=0; j<10; i++, j++)          			// выковыриваем долготу из
                tmp[j]=gps_str[i];              			// из gps_str
        dolgota=atof(tmp);                      			// и преобразуем её в число (double)
	printf("\n%s", gps_str);					// напечатаем прочитанную строку
	fclose(fp);							// закроем порт
}

void clear_gps_str()
{
	int i;
	for(i=0; i<100; i++)
	{
		gps_str[i] = '0';
	}
}

void get_config()
{
	char tmp[25];							// временный массив
	if ((fconfig = fopen(PATH_CONFIG, "a+")) == NULL)
	{
		printf("Не удалось открыть файл конфигурации.\n");
	}
	else
	{
		while(!fscanf(fconfig, "serial = %s", tmp))		// пока в config-файле не встретится строка serial = "
		{
			fgetc(fconfig);					// увеличиваем указатель текущей позиции файла
		}
	loc_data.ln = atoi(tmp);					// считываем номер локомотива
        }
	fclose(fconfig);                                                // закрываем файл конфигурации АСК
}

