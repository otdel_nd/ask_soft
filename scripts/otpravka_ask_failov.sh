#!/bin/sh
USERNAME="fedotov"
PASSWORD="fed11651382-"
SERVER="88.86.83.22"
FTP_PORT=21
REMOTE_DIR="ASK2/podovalov/2TE25KM-999B/2TE25KM-999B-"
ASK_FILES_DIR="/mnt/sdcard/ASK_FILES"
ASK_WORK_DIR="/mnt/sdcard/ASK_FILES/WORK"
ASK_FILE_SIZE="293400c"
INET_STATUS_FILE="/root/podovalov/inet_status"
MAIN_LOOP_INTERVAL=1

clean_work_directory() {
        local RESULT=`find $ASK_WORK_DIR -size $ASK_FILE_SIZE`
        if [ "$RESULT" = "" ]; then
                echo "Work directory is empty."
        else
                cp `find $ASK_WORK_DIR -size $ASK_FILE_SIZE` $ASK_FILES_DIR
                rm `find $ASK_WORK_DIR -size $ASK_FILE_SIZE`
        fi

}

find_ask_files() {
        local RESULT=`find $ASK_FILES_DIR -name "*.ask" -maxdepth 1`
        if [ "$RESULT" = "" ]; then
                echo 1
        else
                echo 0
        fi
}

find_gz_files() {
        local RESULT=`find $ASK_FILES_DIR -name "*.ask.gz" -maxdepth 1`
        if [ "$RESULT" = "" ]; then
                echo 1
        else
                echo 0
        fi
}

check_inet_status() {
        local RESULT=`cat $INET_STATUS_FILE`
        echo $RESULT
}

main_loop() {
        local inet_status
        local ask_status
        local gz_status

        while [ true ]; do
                sleep $MAIN_LOOP_INTERVAL

                clean_work_directory

                ask_status=$(find_ask_files)
                if [ $ask_status -ne 0 ]; then
                        echo "*.ask files not found."
                else
                        echo "*.ask files found. Compressing..."
                        gzip $ASK_FILES_DIR/*.ask
                fi

                inet_status=$(check_inet_status)

                if [ $inet_status -ne 0 ]; then
                        echo "Connection check failed."
                else
                        gz_status=$(find_gz_files)
                        if [ $gz_status -ne 0 ]; then
                                echo "Directory does not contain .ask.gz archive file."
                                continue
                        else
                                local DATE_DAY=$(expr substr $(ls -t1 $ASK_FILES_DIR | grep .ask.gz | head -n 1) 18 2)
                                local DATE_MONTH=$(expr substr $(ls -t1 $ASK_FILES_DIR | grep .ask.gz | head -n 1) 20 2)
                                ncftpput -DD -m -z -u "$USERNAME" -p "$PASSWORD" -P "$FTP_PORT" $SERVER $REMOTE_DIR$(date +"%Y")$DATE_MONTH$DATE_DAY $ASK_FILES_DIR/*_*_$DATE_DAY$DATE_MONTH*.ask.gz
                                EXIT_V="$?"
                                if [ $EXIT_V -ne 0 ]; then
                                        echo "Error: $EXIT_V"
                                else
                                        echo "Well done"
                                fi
                        fi
                fi
        done
}

main_loop
