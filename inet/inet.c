#include <stdio.h>
#include <curl.h>

#include <deque>								// для очереди

#include <shmstorage.h>
#include <typedefs.h>
#include <inet.h>

#define MAX_POST_DATA_BUFFER 120						// количество хранимых 30-секундных записей в циклическом буфере

using namespace std ;

SHMStorage <post_data_t>* postSharedData ;

int post_send( char* ) ;							// функция отправляющая post строку
int check_inet( void ) ;							// проверка интернета

CURL *curl;

int inet( )
{
	post_data_t post_data_record ;						// структура для хранения текущей post строки
	postSharedData = new SHMStorage <post_data_t>("post_data") ;		// для работы с разделяемой памятью

	deque<post_data_t> post_data_buffer ;					// очередь с двусторонним доступом для буфера записей

	while(1)
	{	
		sleep(30) ;							// каждые 30 секунд
		post_data_record = postSharedData->get() ;			// получим текущую post строку из разделяемой памяти
		
		if(post_data_buffer.size() < MAX_POST_DATA_BUFFER)		// если размер очереди не достиг максимального
		{
			post_data_buffer.push_back(post_data_record) ;		// сохраним текущую post строку в очередь
//			print_log("\nРазмер очереди: %d\n", post_data_buffer.size()) ;
		}
		else								// если достиг
		{
//			print_log("\nРазмер полной очереди: %d\n", post_data_buffer.size()) ;
			post_data_buffer.pop_front() ;				// удаляем самую старую строку
			post_data_buffer.push_back(post_data_record) ;		// сохраним текущую post строку в очередь
		}
		
		if( check_inet() == 0 )						// если интернет есть
		{								// отправляем текущую post строку
			if( post_send((char*)post_data_buffer.back().post_data) == CURLE_OK )	
			{
				print_log("\nОтправлена текущая POST строка: %s\n", (char*)post_data_buffer.back().post_data ) ;
				post_data_buffer.pop_back() ;			// удаляем отправленную строку из очереди
			}

                        if( !post_data_buffer.empty() )
                                continue ;
				
			while( check_inet() == 0 && !post_data_buffer.empty() )	// пока очередь не будет пуста или пока есть интернет
			{
				post_send((char*)post_data_buffer.back().post_data) ;
				post_data_buffer.pop_back() ;			// отправляем и опустошаем очередь
//				print_log("\nРазмер после удаления: %d\n", post_data_buffer.size()) ;
			} ;
		}
	} ;
	return 0 ;
}

int post_send( char* post_data )
{
	CURL *curl;								// дескриптор curl
	CURLcode res;								// перечисление типа CURLcode для хранения кода результата отправки
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();						// открываем сессию, curl - easy heandle для использования в остальных функциях easy curl
		
	if(curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL,
				 "http://ask-vnikti.ru/connect.php");		// устанавливаем параметры предстоящей передачи - адрес, куда передаем
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE,
				 (long)strlen(post_data) );			// длина строки
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);		// строка для отправки методом POST
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3L);			// пытаться соединиться не дольше 3 секунд
	//	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);			// (для отладки)
		res = curl_easy_perform(curl);					// отправляем данные, результат отправки присваиваем res
	        if(res != CURLE_OK)						// анализируем на предмет отсутствия ошибок
		{
			curl_easy_cleanup(curl);				// очищаем сессию
			return res ;
		}

		curl_easy_cleanup(curl);					// очищаем сессию
		return res ;
	}
}

int check_inet( void )
{
	int inet_status ;
	FILE* inet_status_file ;
	if( (inet_status_file = fopen("/tmp/run/inet_status", "r") ) == NULL )
	{
		print_log ("Не удалось открыть файл /tmp/run/inet_status") ;
		exit(1) ; 
	}
	fscanf(inet_status_file, "%d", &inet_status) ;

	fclose(inet_status_file) ;

	return inet_status ;
} ;
