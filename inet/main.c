#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#include <inet.h>

void cmd_line ( int argc, char *argv[] ) ;

int main( int argc, char * argv[] )
{
	pid_t iChild ;					// для хранения идентификатора процесса

	cmd_line( argc, argv ) ;			// проверяем и обрабатываем параметры командной строки

	if ( IsDaemon )					// если ключ --daemon использовался (запускаем как демон)
	{
		print_log( "Стартуем..." ) ;

		iChild = fork() ;			// теперь выполняется два процесса
		if ( iChild < 0 )
		{
			print_log( "Ошибка запуска сервера: %d.", errno ) ;
			return 1 ;
		}

		if ( iChild )	 			// родительский процесс завершаем, дочерний процесс будет демоном (процессом сиротой)
		{
			print_log( "Cервер запущен в режиме демона" ) ;
			return 0 ;
		}
	}

	return inet( ) ;
}
