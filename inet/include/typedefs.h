#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <cstdint>
#include <string>

extern "C" {
#include <embtime.h>
}

typedef std::basic_string<uint8_t> byte_array;

typedef float float32_t;

//! Коды ошибок
enum ERROR_CODE : uint8_t {
    E_OK                            = 0x00, /*!< Успешное завершение */
    E_ALLOC_ERROR                   = 0x01, /*!< Ошибка выделения памяти */
    E_TIMEOUT                       = 0x02,
    E_NULL                          = 0x03,
    E_DEVICE_NOT_PRESENT            = 0x04,
    E_CHECKSUM_ERROR                = 0x05,
    E_CLOCK_ERROR                   = 0x06,
    E_FAIL                          = 0x07,
    E_INVALID_DATA                  = 0x08,
    E_LEN_ERROR                     = 0x09,
    E_BUFFER_OVERFLOW               = 0x0A,
    E_BUFFER_UNDERFLOW              = 0x0B,
    E_INVALID_LENGTH                = 0x0C,
    E_SYNC_ERROR                    = 0x0D,
    E_LOCK_ERROR                    = 0x0E,
    E_INVALID_ADDRESS               = 0x0F,
    E_TOO_LATE                      = 0x10,
    E_DEVICE_BUSY                   = 0x11,
    E_PORT_CLOSED                   = 0x12,
    E_WRITE_ERROR                   = 0x13,
    E_READ_ERROR                    = 0x14,
    E_INVALID_ANSWER                = 0x15,
    E_DB_OPEN_ERROR                 = 0x16,
    E_DB_ERROR                      = 0x17,
    E_MORE_BYTES                    = 0x18,
    E_SOCKET_ERROR                  = 0x19,
    E_HOSTNAME_ERROR                = 0x1A,
    E_CONNECT_ERROR                 = 0x1B
};

typedef struct __attribute__ ((__packed__)) {
    uint8_t flags;
    // кол-во секунд с 00:00:00 01-01-2010
    embtime_t time;
    // 24 бита (значение XXXX.XXXX >> 3)
    uint8_t latitude[3];
    // 24 бита (значение XXXX.XXXX >> 3)
    uint8_t longitude[3];
    uint8_t speed;
    // значене курса в градусах >> 1
    uint8_t course;
} gps_data_record_t;

typedef struct __attribute__ ((__packed__)) {
    size_t  response_len;
    uint8_t read_result;
} msu_data_record_t;

#define BUF_SIZE 512							// длина IPX пакета
typedef struct __attribute__ ((__packed__)) {
    unsigned char data_buf[BUF_SIZE];
    int zer_size, zer_block_size, upravl_size ;
} ipx_data_visit_t;

typedef struct __attribute__ ((__packed__)) {
    unsigned char data_buf[BUF_SIZE];
} ipx_data_data_t;

typedef struct __attribute__ ((__packed__)) {
    uint8_t msg_flag;
    uint16_t msg_counter;
    uint32_t msg_time;
    uint32_t msg_rand;
    uint8_t msg_text[59];
} data_message_t;

typedef struct __attribute__ ((__packed__)) {
    uint8_t post_data[400];
} post_data_t;

#define GPS_DATA_RECORD_COORD_SHIFT         (3)
#define GPS_DATA_RECORD_COORD_DEG_DIV       (1000000UL)
#define GPS_DATA_RECORD_COORD_MIN_DIV       (10000UL)
#define GPS_DATA_RECORD_COORD_SEC_DIV       (100UL)

typedef struct __attribute__ ((__packed__)) {
    float32_t   left_sensor_raw;
    float32_t   right_sensor_raw;
    uint16_t    fuel_weight;
    int8_t      fuel_temperature;
} fuel_data_record_t;

enum GPS_DATA_RECORD_FLAGS : uint8_t {
    GPS_DATA_RECORD_FLAGS_VALID		=	1 << 0,
    GPS_DATA_RECORD_FLAGS_ERROR     =	1 << 1,
    GPS_DATA_RECORD_FLAGS_N         =	1 << 4,
    GPS_DATA_RECORD_FLAGS_E         =	1 << 5
};

#endif // TYPEDEFS_H
