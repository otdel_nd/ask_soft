#ifndef INET_H
#define INET_H

extern int IsDaemon ;					// глобальная переменная объявлена в другом файле (cmd_line.c)

int print_log( const char *fmt, ... ) ;
int inet( void ) ;

#endif // INET_H
