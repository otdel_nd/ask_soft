#!/bin/bash
# Cборка программ для новой платы ASK
# в качестве параметра передаём имя программы, как она будет называться на плате
COUNT_STR_ERR=0
if [ $# -lt 1 ]
then
	echo $0 не удалось: нужно задать аргумент \(имя программы\)
else
	cd build
	cmake -DCMAKE_TOOLCHAIN_FILE=../../../acs_software/som/cmake/buildroot.cmake .. 1> /dev/null
	make 2> error.tmp 1> /dev/null

	COUNT_STR_ERR=$(wc -l error.tmp)

	if [ "$COUNT_STR_ERR" != "0 error.tmp" ]
	then
		echo Ошибки сборки:
		cat error.tmp
		rm -f error.tmp
		make clean 1> /dev/null
	else
		echo Сборка программы прошла  успешно, отправляем файл на АСК
		scp ${1%.*} root@10.0.2.214:/root/podovalov
		cp $1 /home/user/devel/acs_som_sdk/psp/buildroot-2013.05/system/overlay/usr/sbin/$1
		rm -f error.tmp
		rm -f $1
		make clean 1> /dev/null
	fi
fi
