#include <cassert>
#include <iostream>
#include <ctime>

#include <sys/socket.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>


extern "C" {
#include <sys/time.h>
}


#include "ustapollcontroller.h"

#define MSU_DEFAULT_DATA_QUEUE_SIZE     1024

USTAPollController::USTAPollController(const std::string &askFilesPath, uint16_t maxCountRecords, uint32_t locNum) :
    device(NULL), locNum(locNum), fuelSharedData("fuel_data"),
    gpsSharedData("gps_data"), postSharedData("post_data"),
    isRunning(false)
{
    // Создание экземпляра класса-драйвера МСУ
    device = new USTADeviceDriver();
    assert(device != NULL);

    // Создание экземпляра класса-очереди незаписанных в файл .ask данных (с указанием максимального размера очереди)
    dataSendingQueue = new ASKFilesWriter::WorkerDataQueue(MSU_DEFAULT_DATA_QUEUE_SIZE);
    assert(dataSendingQueue != NULL);

    // Создание экземпляра писателя данных из очереди сообщений в файл .ask
    filesWriter = new ASKFilesWriter(locNum, askFilesPath, maxCountRecords, dataSendingQueue);
    assert(filesWriter != NULL);

    // Запуск потока извлечения данных из очереди и записи в файл
    filesWriter->startWritingLoop();
}

USTAPollController::~USTAPollController() {
    isRunning = false;

    dataSendingQueue->clear();

    if (filesWriter != NULL) {
        filesWriter->stop();
        delete filesWriter;
    }

    if (dataSendingQueue != NULL) {
        delete dataSendingQueue;
    }

    if (device != NULL) {
        device->closePort();
        delete device;
    }
}

ERROR_CODE USTAPollController::openPort(std::string port, uint32_t speed) {
    return device->openPort(port, speed);
}

void USTAPollController::start(int timeout) {
    uint8_t res;
    fuel_data_record_t fuelData;
    gps_data_record_t gpsData;
    post_data_t postData;
    byte_array ustaData;
    byte_array aluData;

    // Создание и заполнений типовой структуры для хранения неотправленных данных в очереди на запись в файл
    ASKFilesWriter::worker_data_rec_t workerDataRecord;

    isRunning = true;

    // Опрос УСТА пока не будет вызван метод stop()
    /*
     * Данные о текущих показаниях топливного датчика и GPS считываются и отправляются в виде запроса на УСТА.
     * После чего считывается ответ от УСТА и формируется структура с ответом УСТА, временем получения сообщения, итп.
     * Далее ответ от УСТА должен формироваться в строку для отправки на сайт.
     */
    while (isRunning) {
        // Задержка между опросами
        sleep(timeout);

        gpsData.speed=99;
        // Получение текущих значений от программ работы с GPS и топливными датчиками посредством разделяемой памяти
        gpsData = gpsSharedData.get();
        fuelData = fuelSharedData.get();

        printf("\ngpsData.speed=%d\n", gpsData.speed);

        // Отправка запроса на АЛУ
        res = device->sendALURequest();
        if (res != E_OK) {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Не удалось отправить запрос на АЛУ. Ошибка: " << res;
        }
        // Очистка переменной с данными перед чтением ответа
        aluData.clear();
        // Чтение ответа от АЛУ
        res = device->readResponse(aluData);
        if (res != E_OK) {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Не удалось прочитать запрос с АЛУ. Ошибка: " << res;
            for(int i=0;i<40;i++)
              aluData.push_back(0);
        }
        else {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Чтение запроса с АЛУ успешно. Размер: " << aluData.size();
        }

        // Отправка запроса на УСТА с данными AЛУ и топливных датчиков
        res = device->sendUSTARequest(aluData, fuelData);
        if (res != E_OK) {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Не удалось отправить запрос на УСТА. Ошибка: " << res;
        }
        // Очистка переменной с данными перед чтением ответа
        ustaData.clear();
        // Чтение ответа от УСТА
        res = device->readResponse(ustaData);
        if (res != E_OK) {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Не удалось прочитать запрос с УСТА. Ошибка: " << res;
            for(int i=0;i<39;i++)
              ustaData.push_back(0);
        }
        else {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Чтение запроса с УСТА успешно. Размер: " << ustaData.size();
        }

        // Установка новых считанных данных в структуру для помещения в очередь на отправку на сервер
        workerDataRecord.data = ustaData;
        workerDataRecord.fuelData = fuelData;
        workerDataRecord.gpsData = gpsData;

        // Установка текущей временной метки (время считывания данных от УСТА)
        workerDataRecord.timestamp = time(NULL);
        // Добавление новой сформированной записи с данными от МСУ в очередь на отправку на сервер сбора данных
        // Далее из очереди данные будет забирать WorkerPacketSender (packetSender) и отправлять их на сервер в параллельном потоке
        dataSendingQueue->push_noblock(workerDataRecord);

        // Формирование post строки с данными УСТА, GPS и топлива
        postData = constructPostQuery(aluData, ustaData, gpsData, fuelData, locNum);
        // помещение post строки в разделяемую память
        postSharedData.set(postData);

     }

}

post_data_t USTAPollController::constructPostQuery(const byte_array &aluData, const byte_array &ustaData, const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData, uint32_t locNum) {
   post_data_t postData;
   char post_data[400];
   char tmp[120];
   struct tm *ptime;
   time_t lt;

   memset( post_data, '\0', 400 );

   lt = time(NULL)+10800;
   ptime = localtime(&lt);
   //ptime->tm_hour<21?ptime->tm_hour += 3:ptime->tm_hour==21?ptime->tm_hour=0:ptime->tm_hour==22?ptime->tm_hour=1:ptime->tm_hour==23?ptime->tm_hour=2:ptime->tm_hour==24?ptime->tm_hour=3:printf("Такого не бывает!");

   strcat(post_data, "dt=");
   strftime(tmp, 25, "%Y-%m-%d %H:%M:%S", ptime);
   strcat(post_data, tmp);

   strcat(post_data, "&ln=");
   sprintf(tmp, "%d", locNum);
   strcat(post_data, tmp);

   strcat(post_data, "&fuel=");
   sprintf(tmp, "%d", fuelData.fuel_weight);
   strcat(post_data, tmp);

   strcat(post_data, "&position=");
   sprintf(tmp, "%d", ustaData[31]);
   strcat(post_data, tmp);

   strcat(post_data, "&speed=");
   sprintf(tmp, "%d", gpsData.speed);
   strcat(post_data, tmp);

   strcat(post_data, "&rpm=");
   sprintf(tmp, "%d", (ustaData[16]<<8 | ustaData[17]));
   strcat(post_data, tmp);

   double rez, c3, c6 ; int c1, c2, c4, c5 ;
   rez = (gpsData.longitude[2] << 16 | gpsData.longitude[1] << 8 | gpsData.longitude[0]) << 3 ;
   c4 = (int)(rez/1000000) ;
   c5 = (int)((rez-(((int)(rez/1000000))*1000000))/10000) ;
   c6 = ((((rez/10000)-((int)(rez/10000)))*100000)*60)/100000 ;

   rez = (gpsData.latitude[2] << 16 | gpsData.latitude[1] << 8 | gpsData.latitude[0]) << 3 ;
   c1 = (int)(rez/1000000) ;
   c2 = (int)((rez-(((int)(rez/1000000))*1000000))/10000) ;
   c3= ((((rez/10000)-((int)(rez/10000)))*100000)*60)/100000 ;

   strcat(post_data, "&c1=");
   sprintf(tmp, "%d", c1);
   strcat(post_data, tmp);

   strcat(post_data, "&c2=");
   sprintf(tmp, "%02d", c2);
   strcat(post_data, tmp);

   strcat(post_data, "&c3=");
   sprintf(tmp, "%f", c3);
   strcat(post_data, tmp);

   strcat(post_data, "&c4=");
   sprintf(tmp, "%d", c4);
   strcat(post_data, tmp);

   strcat(post_data, "&c5=");
   sprintf(tmp, "%02d", c5);
   strcat(post_data, tmp);

   strcat(post_data, "&c6=");
   sprintf(tmp, "%f", c6);
   strcat(post_data, tmp);

   struct ifreq ifr;
   memset(&ifr, 0, sizeof(ifr));
   strcpy(ifr.ifr_name, "ppp0");
   int s = socket(AF_INET, SOCK_DGRAM, 0);
   ioctl(s, SIOCGIFADDR, &ifr);
   close(s);
   struct sockaddr_in *sa = (struct sockaddr_in*)&ifr.ifr_addr;
   strcat(post_data, "&a1=");
   strcat(post_data, inet_ntoa(sa->sin_addr));

   for(int i=0 ; i<400 ; i++)
     postData.post_data[i] = post_data[i];

   return postData;
}
