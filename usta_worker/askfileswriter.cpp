#include "askfileswriter.h"

#include <string.h>
#include <iostream>
#include <cassert>
#include <typedefs.h>
#include <stdio.h>
#include <stdlib.h>

ASKFilesWriter::ASKFilesWriter(uint32_t locNum, const std::string &askFilesPath, uint16_t maxCountRecords, WorkerDataQueue *dataQueue) :
  locNum(locNum), askFilesPath(askFilesPath), maxCountRecords(maxCountRecords), dataQueue(dataQueue), isRunning(false),
  writingLoopThread(NULL)
{
  assert(dataQueue != NULL);
}

ASKFilesWriter::~ASKFilesWriter()
{
  isRunning = false;

  if (writingLoopThread != NULL) {
      if (writingLoopThread->joinable()) {
          writingLoopThread->join();
      }
      delete writingLoopThread;
      writingLoopThread = NULL;
  }
}

uint8_t ASKFilesWriter::startWritingLoop() {
    if (isRunning) {
        return E_FAIL;
    }

    writingLoopThread = new std::thread(ASKFilesWriter::writingLoop, this);

    return E_OK;
}

void ASKFilesWriter::stop() {
    isRunning = false;

    if (writingLoopThread != NULL) {
        worker_data_rec_t dummy;
        dataQueue->push_noblock(dummy);
        writingLoopThread->join();
    }
}

void ASKFilesWriter::writingLoop(ASKFilesWriter *protoClient) {
    FILE *fask;
    char file_name[100];
    int count_records = 0;

    // признак начала зеркала
    uint16_t FF0F = 0x0FFF ;
    worker_data_rec_t record;

    assert(protoClient != NULL);
    protoClient->isRunning = true;

    //! пока объект(this) вызвавший поток существует
    while(protoClient->isRunning) {
        // Получение элемента из очереди
        record = protoClient->dataQueue->pop();
        std::cout << "Размер очереди: " << protoClient->dataQueue->size() << std::endl;
        sleep(1);

	// если предыдущее количество записей равно 0 (то есть предыдущий файл переполнен или не существует)
	if ( count_records == 0 ) {
	    memset( file_name, '\0', 50 );
	    protoClient->make_file_name( file_name, protoClient->askFilesPath, protoClient->locNum );

	    fask = fopen( file_name, "wb" );

	    assert(fask != NULL);

	    // запишем в файл зеркало
	    fwrite( &FF0F, sizeof(unsigned char), 2, fask );
	    fwrite( record.data.c_str(), sizeof(unsigned char), record.data.size(), fask );

	    count_records++;
	    fclose(fask);
	}

	// если файл уже существует
	else {
	    // a число записей еще не достигло максимального значения
	    if ( count_records < protoClient->maxCountRecords-1 ) {

		// откроем файл для записи
		fask = fopen( file_name, "rb+" );
		assert(fask != NULL);

		// перед записью в файл переместимся в конец файла
		fseek( fask, 0, SEEK_END );
		// запишем в файл зеркало
		fwrite( &FF0F, sizeof(unsigned char), 2, fask );
		fwrite( record.data.c_str(), sizeof(unsigned char), record.data.size(), fask );
		count_records++;
		fclose(fask);
	    }

	    // если число записей равно максимальному значению-1
	    else {
		fask = fopen( file_name, "rb+" );
		assert(fask != NULL);

		// перед записью в файл переместимся в конец файла
		fseek( fask, 0, SEEK_END ) ;
		// запишем последнее зеркало в файл
		fwrite( &FF0F, sizeof(unsigned char), 2, fask );
		fwrite( record.data.c_str(), sizeof(unsigned char), record.data.size(), fask );
		count_records = 0;
		fclose(fask);
	   }
	}
    }

}

void ASKFilesWriter::make_file_name(char *file_name, const std::string &askFilesPath, uint32_t locNum) {
  char tmp[50];
  struct tm *ptime;
  time_t lt;

  // путь к папке ASK_FILES
  strcat(file_name, askFilesPath.c_str());

  // добавим в имя файла тип тепловоза и номер
  switch ((int)locNum/100000) {
      case 510 : strcat( file_name, "TEP70BS_" ) ; break ;
      case 606 : strcat( file_name, "2TE116U_") ; break ;
      case 608 : strcat( file_name, "2TE25A_") ; break ;
      case 696 : strcat( file_name, "2TE25KM_") ; break ;
      case 530 : strcat( file_name, "2TE116_") ; break ;
  }
  sprintf(tmp, "%d", locNum);
  strcat( file_name, tmp );
  strcat( file_name, "_" );

  // а также время и дату сбора данных
  lt = time(NULL)+10800;
  ptime = localtime(&lt);
  strftime( tmp, 25, "%d%m%H%M", ptime );
  strcat( file_name, tmp );

  // и расширение
  strcat( file_name, ".ask" );
}
