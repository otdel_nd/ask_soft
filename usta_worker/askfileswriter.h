#ifndef ASKFILESWRITER_H
#define ASKFILESWRITER_H

#include <cstdint>
#include <thread>
#include <mutex>
#include <string>
#include <atomic>
#include <list>
#include <unordered_map>

#include <typedefs.h>

#include <blockingqueue.h>

//! Класс для фоновой записи данных из очереди сообщений в файл .ask
/*!
 * Данный класс получает данные из очереди на отправку \sa WorkerDataQueue
 * и в отдельном потоке записывает их в файл .ask
 */
class ASKFilesWriter
{
public:
    //! Структура для хранения записи с данными в очереди на отправку
    typedef struct {
        fuel_data_record_t          fuelData;
        gps_data_record_t           gpsData;
        rec_timestamp_t             timestamp;      /*!< дата и время получения данных от устройства */
        byte_array                  data;           /*!< непосредственно данные, полученные от устройства */
    } worker_data_rec_t;

    typedef BlockingQueue<worker_data_rec_t> WorkerDataQueue;

    /*!
     * \brief Конструктор класса
     * \param askFilesPath пусть к каталогу размещения .ask файлов
     * \param maxCountRecords максимальное число записей в одном файле
     * \param dataQueue указатель на очередь с записями для записи в файл
     */
    ASKFilesWriter(uint32_t locNum, const std::string &askFilesPath, uint16_t maxCountRecords, WorkerDataQueue *dataQueue);

    //! Виртуальный деструктор
    virtual ~ASKFilesWriter();

    //! Запустить поток записи в файл .ask
    uint8_t startWritingLoop();

    //! Остановить поток записи в файл .ask
    void stop();

protected:

    //! Функция-поток для записи сообщений в файл
    static void writingLoop(ASKFilesWriter *protoClient);

    //! Отправить пакет с данными на сервер
    uint8_t sendDataRecord(const worker_data_rec_t &record);

    static uint8_t setServerAddress(struct sockaddr_in &sockAddr, const std::string &serverHost);

    // функция формирует имя файла
    void make_file_name(char *file_name, const std::string &askFilesPath, uint32_t locNum);

private:

    uint32_t locNum;
    std::string askFilesPath;
    uint16_t maxCountRecords;

    WorkerDataQueue *dataQueue;



    std::atomic<bool> isRunning;

    std::thread *writingLoopThread;
};

#endif // ASKFILESWRITER_H

