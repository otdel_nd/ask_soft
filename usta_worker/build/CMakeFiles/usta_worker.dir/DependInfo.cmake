# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/user/devel/acs_software/som/common/src/embtime.c" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/home/user/devel/acs_software/som/common/src/embtime.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/user/devel/ask_soft/usta_worker/askfileswriter.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/askfileswriter.cpp.o"
  "/home/user/devel/acs_software/som/common/src/networkprotopacketgenerator.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/home/user/devel/acs_software/som/common/src/networkprotopacketgenerator.cpp.o"
  "/home/user/devel/acs_software/som/common/src/networkprotoparser.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/home/user/devel/acs_software/som/common/src/networkprotoparser.cpp.o"
  "/home/user/devel/ask_soft/usta_worker/main.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/main.cpp.o"
  "/home/user/devel/ask_soft/usta_worker/ustadevicedriver.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/ustadevicedriver.cpp.o"
  "/home/user/devel/ask_soft/usta_worker/ustapollcontroller.cpp" "/home/user/devel/ask_soft/usta_worker/build/CMakeFiles/usta_worker.dir/ustapollcontroller.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  ".."
  "../../../acs_software/som/common/include"
  "../../../acs_software/som/libs/serialportio"
  "../../../acs_software/som/libs/serialportio/logging_serialportio"
  "../../../acs_software/som/libs/serialportio/unix_serialportio"
  "../../../acs_software/som/libs/elogger"
  "../../../acs_software/som/libs/libini"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
