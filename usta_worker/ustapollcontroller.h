#ifndef USTAPOLLCONTROLLER_H
#define USTAPOLLCONTROLLER_H

#include <atomic>

#include <typedefs.h>
#include <shmstorage.h>
#include <ustadevicedriver.h>
#include <askfileswriter.h>

//! Класс контроллера опроса УСТА и АЛУ
class USTAPollController
{
public:
    /*!
     * \brief Конструктор класса
     * \param
     * \param
     */
    USTAPollController(const std::string &askFilesPath, uint16_t maxCountRecords, uint32_t locNum);

    ~USTAPollController();

    /*!
     * \brief функция открытия порта для работы с МСУ
     * \param port путь к файлу устройства
     * \param speed скорость работы порта
     * \return код ошибки
     */
    ERROR_CODE openPort(std::string port, uint32_t speed);

    //! Функция опроса УСТА и АЛУ
    /*! Опрос выполняется в вызывающем потоке. Функция блокируется до вызова функции stop из другого потока */
    void start(int timeout);

    //! Прервать цикл опроса УСТА и АЛУ
    void stop() { isRunning = false; }

    post_data_t constructPostQuery( const byte_array &aluData, const byte_array &ustaData, const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData, uint32_t locNum) ;

private:
    //! Указатель на очередь сообщений для записи в файл .ask
    ASKFilesWriter::WorkerDataQueue *dataSendingQueue;
    //! Указатель на экземпляр класса, занимающегося записью данных в файл .ask
    ASKFilesWriter *filesWriter;
    //! Указатель на экземпляр класса-драйвера УСТА и АЛУ
    USTADeviceDriver *device;
    uint32_t locNum;

    //! Экземпляр класса-хранилища (в разделяемой между приложениями памяти) для получения текущий данных по топливу
    /*!
        Данный класс исользуется для получения данных от приложения работы с топливными датчиками. Данные нужны для отправки в МСУ.
        Получение данных идёт при помощи разделяемой памяти (SHM).
     */
    SHMStorage<fuel_data_record_t> fuelSharedData;
    //! Экземпляр класса-хранилища (в разделяемой между приложениями памяти) для получения данных по текущему состоянию GPS
    /*!
        Данный класс исользуется для получения данных от приложения работы с GPS. Данные нужны для отправки в МСУ.
        Получение данных идёт при помощи разделяемой памяти (SHM).
     */
    SHMStorage<gps_data_record_t> gpsSharedData;

    SHMStorage<post_data_t> postSharedData;

    std::atomic<bool> isRunning;
};

#endif // USTAPOLLCONTROLLER_H
