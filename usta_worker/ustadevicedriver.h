#ifndef USTADEVICEDRIVER_H
#define USTADEVICEDRIVER_H

#include <cstdint>
#include <string>

#include <typedefs.h>

#include <iserialportio.h>

//! Класс-драйвер для работы с УСТА
class USTADeviceDriver
{
public:
    //! Структура с описанием формата запроса данных от УСТА (содержит значения АЛУ и топливных датчиков)
    typedef struct __attribute__ ((__packed__)) {
        uint8_t             command;
        uint16_t            fuel_weight;
        uint16_t            oil_temperature;
        uint16_t            water_temperature;
        uint16_t            oil_pressure;
    } usta_device_request_data_t;

    typedef struct __attribute__ ((__packed__)) {
        uint16_t            voltage_VU;
        uint16_t            amperage_VU;
        uint16_t            voltage_bortovoy_seti;
        uint16_t            tabel_number;
        uint16_t            marshrut_list_number;
        uint16_t            power_generator_zadanie;
        uint16_t            voltage_generator;
        uint16_t            weight_of_the_train;
        uint16_t            chastota_vra_kolenvala;
        uint16_t            plotnost_fuel;
        uint16_t            ugol_SHIM_1;
        uint16_t            ugol_SHIM_2;
        uint16_t            regim_ekspluatacii;
        uint16_t            pologenie_induk_dat;
        uint16_t            kod_uchastka;
        uint16_t            position_kontrollera;
        uint16_t            power_generator;
        uint8_t             byte35;
        uint8_t             byte36;
        uint8_t             byte37;
        uint8_t             byte38;
        uint8_t             byte39;
    } usta_device_response_data_t;

    //! Перечисление значений возможных служебных байт
    enum USTA_PROTO_BYTES : uint8_t {
        USTA_PROTO_BYTE_ESC          =   0xFE,   /*!< Экранирующий символ */
        USTA_PROTO_BYTE_START        =   0xFF,    /*!< Байт синхронизации */
        ALU_PROTO_BYTE_START        =   0xFF    /*!< Байт синхронизации */
    };

    //! Перечисление возможных состояний GPS
    enum USTA_PROTO_GPS_STATUS : uint8_t {
        USTA_PROTO_GPS_STATUS_NONE   =   0x00,
        USTA_PROTO_GPS_STATUS_2D     =   0x01,
        USTA_PROTO_GPS_STATUS_3D     =   0x02
    };

    USTADeviceDriver();

    ~USTADeviceDriver();

    //! Открыть порт УСТА
    ERROR_CODE openPort(std::string port, uint32_t speed);

    //! Закрыть порт устройства
    void closePort() {
        portIO->close();
    }
    /*!
     * \brief Отправка запроса на АЛУ.
     * \param без параметров
     * \return код ошибки
     */
    ERROR_CODE sendALURequest();

    /*!
     * \brief Отправка запроса на УСТА с текущим состоянием системы (АЛУ и топливо).
     * \param gpsData текущие данные GPS
     * \param fuelData текущие данные по топливу
     * \return код ошибки
     */
    ERROR_CODE sendUSTARequest(const byte_array &aluData, const fuel_data_record_t &fuelData);

    /*!
     * \brief Считать ответ от УСТА
     * \param responsе массив байт, содержащий ответ от УСТА. Без байта синхронизации, без байта длины пакета и без байта контрольной суммы. Без "байт-стаффингов" (экранирующих символов).
     * \return код ошибки
     */
    ERROR_CODE readResponse(byte_array &response);

protected:

    //! Функция формирования посылки для УСТА с указанием текущих значений GPS и топлива
    void constructRequestPacket(usta_device_request_data_t &packet, const byte_array &aluData, const fuel_data_record_t &fuelData);

    //! Добавление экранирующих байт
    byte_array escapeBuffer(const byte_array &buf);

private:
    //! Указатель на экземпляр класса для работы с последовательным портом
    ISerialPortIO *portIO;
};

#endif // USTADEVICEDRIVER_H
