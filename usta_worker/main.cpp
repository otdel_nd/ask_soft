#include <iostream>
#include <cassert>
//#include <string>
//#include <cstring>
#include <inireader.h>
#include <elogger.h>

#include <ustapollcontroller.h>

using namespace std;
//! Cтруктура с параметрами порта и т.п.
struct globalArgs_t {
    std::string             askFilesPath;
    uint16_t                maxCountRecords;
    uint32_t                locNum;
    uint32_t                device_speed;
    std::string             device_path;
    int                     timeout;
    std::string             configPath;
} globalArgs;

USTAPollController *pollController = NULL;

void initLogger() {
    ELogger::initLogger();

    ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_INFO) << "usta_worker started";
}

int main(int argc, char *argv[]) {
    initLogger();

    uint8_t res;

    // Задание значений опций по умолчанию
    globalArgs.device_path = "/dev/ttySU1";
    globalArgs.device_speed = 19200;
    globalArgs.timeout = 1;
    globalArgs.askFilesPath = "/mnt/sdcard/ASK_FILES/";
    globalArgs.maxCountRecords = 600;
    globalArgs.locNum = 53015492;
    globalArgs.configPath = "/etc/ask/ask.conf";

    INIReader config(globalArgs.configPath);

    if (config.parseResult() == 0) {
       globalArgs.locNum = config.getUInt("ident", "serial", globalArgs.locNum);
       globalArgs.askFilesPath = config.get("storage", "ask_files_path", globalArgs.askFilesPath);
       globalArgs.maxCountRecords = config.getUInt("storage", "max_count_records", globalArgs.maxCountRecords);
    } else {
        ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Ошибка чтения файла конфигурации:" << config.parseResult();
    }

    // Создание экземпляра контроллера опроса УСТА
    pollController = new USTAPollController(globalArgs.askFilesPath, globalArgs.maxCountRecords, globalArgs.locNum);
    assert(pollController != NULL);

    // Открытие порта устройства УСТА
    res = pollController->openPort(globalArgs.device_path, globalArgs.device_speed);
    if (res != E_OK) {
        ELOG(ELogger::INFO_DEVICE, ELogger::LEVEL_ERROR) << "Не могу открыть порт " << globalArgs.device_path << "; Ошибка: " << res;
        delete pollController;
        return 1;
    }

    // Запуск опроса УСТА. Функция передаёт управление дальше только при выходе из программы
    pollController->start(globalArgs.timeout);

    // Очистка ресурсов
    if (pollController != NULL) {
        delete pollController;
    }

    return 0;
}
