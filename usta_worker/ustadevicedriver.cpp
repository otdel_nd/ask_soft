#include "ustadevicedriver.h"

#include <iostream>
#include <cassert>
#include <cstring>

#include <serialportiofabric.h>

#define USTA_PORT_INTERBYTE_TIMEOUT  SERIALPORTIO_CONVERT_MS(100)    // 100mS
#define USTA_PORT_READ_TIMEOUT       SERIALPORTIO_CONVERT_MS(500)    // 500mS
#define USTA_PROTO_CHECKSUM_LEN      1

#define USTA_PROTO_LAT_N             10
#define USTA_PROTO_LAT_S             20
#define USTA_PROTO_LONG_E            1
#define USTA_PROTO_LONG_W            2

#define PACKET_READ_BUF_SIZE        512

#define PACKET_BYTE_START           0xFF
#define PACKET_BYTE_ESC             0xFE
#define PACKET_FIXED_PAYLOAD_LEN    201
#define PACKET_FIXED_FULL_LEN       203
#define PACKET_OVERLENGHT           2U

USTADeviceDriver::USTADeviceDriver() :
    portIO(NULL)
{
    portIO = SerialPortIOFabric::getInstance();
    assert(portIO != NULL);
}

USTADeviceDriver::~USTADeviceDriver() {
    if (portIO != NULL) {
        portIO->close();
        delete portIO;
    }
}


ERROR_CODE USTADeviceDriver::openPort(std::string port, uint32_t speed) {
    int res;

    assert(portIO != NULL);

    // Установка скорости и других параметров последовательного порта
    portIO->setSpeed(speed);
    portIO->setFlowControl(ISerialPortIO::FLOW_OFF);
    portIO->setParity(ISerialPortIO::PAR_EVEN);
    portIO->setStopBits(ISerialPortIO::STOP_2);
    portIO->setInterByteTimeout(USTA_PORT_INTERBYTE_TIMEOUT);
    portIO->setReadTimeout(USTA_PORT_READ_TIMEOUT);

    // Непосредственно открытие порта
    res = portIO->open(port.c_str());

    if (res >= 0) {
        // Применение настроек порта
        portIO->setSettings();
    } else {
        return E_FAIL;
    }

    return E_OK;
}

ERROR_CODE USTADeviceDriver::sendUSTARequest(const byte_array &aluData, const fuel_data_record_t &fuelData) {
    usta_device_request_data_t data;
    byte_array packet;              // Байтовый буфер данных для отправки в УСТА
    uint8_t checksum = 0;
    int bytesWritten;

    // Формирование пакета для отправки данных в УСТА
    constructRequestPacket(data, aluData, fuelData);

    // Добавление байта длины пакета в буфер данных для отправки
    packet.push_back(sizeof(data) + USTA_PROTO_CHECKSUM_LEN);

    // Добавление данных тела пакета в буфер для отправки
    packet.append(reinterpret_cast<const uint8_t *>(&data), sizeof(data));

    // Вычисление контрольной суммы
    for (size_t i = 0; i < packet.size(); i++) {
        checksum += packet.at(i);
    }

    checksum -= 0xFF;
    checksum = (int8_t)((~checksum)+1) ;
    checksum++;

    // Добавление контрольной суммы в буфер для отправки
    packet.push_back(checksum) ;

    // Добавление экранирующих символов в буфер для отправки
    packet = escapeBuffer(packet);

    // Добавление байта синхронизации в начало пакета
    packet.insert(0, 1, USTA_PROTO_BYTE_START);

/*
    printf("\nChecksum USTA: %X \n", checksum ) ;

    std::cerr << "Writing packet: ";

    for (size_t i = 0; i < packet.size(); i++) {
        fprintf(stderr, "%.2X ", packet.at(i));
    }

    std::cerr << std::endl;
*/

    // Запись сформированного буфера в порт
    bytesWritten = portIO->write(packet);

    if ((bytesWritten >= 0) && (bytesWritten == (int)packet.size())) {
        return E_OK;
    } else {
        return E_WRITE_ERROR;
    }
}
ERROR_CODE USTADeviceDriver::sendALURequest() {
    byte_array packet;              // Байтовый буфер данных для отправки в АЛУ
    uint8_t checksum = 0;
    int bytesWritten;

    // Добавление байта синхронизации в начало пакета
    packet.insert(0, 1, ALU_PROTO_BYTE_START);

    // Добавление байта длины пакета в буфер данных для отправки
    packet.push_back(0x0A) ;

    // Добавление данных тела пакета в буфер для отправки
    packet.push_back(0x0D) ;
    uint8_t reserv[8] = {0,0,0,0,0,0,0,0} ;
    packet.append(reinterpret_cast<const uint8_t *>(&reserv), 8);

    // Вычисление контрольной суммы
    for (size_t i = 1; i < packet.size(); i++) {
        checksum += packet.at(i);
    }

    checksum = (int8_t)((~checksum)+1) ;

    // Добавление контрольной суммы в буфер для отправки
    packet.push_back(checksum) ;

/*    printf("\nChecksum ALU: %X \n", checksum ) ;

    std::cerr << "Writing packet: ";

    for (size_t i = 0; i < packet.size(); i++) {
        fprintf(stderr, "%.2X ", packet.at(i));
    }

    std::cerr << std::endl;
*/
    // Запись сформированного буфера в порт
    bytesWritten = portIO->write(packet);

    if ((bytesWritten >= 0) && (bytesWritten == (int)packet.size())) {
        return E_OK;
    } else {
        return E_WRITE_ERROR;
    }
}

ERROR_CODE USTADeviceDriver::readResponse(byte_array &response) {
    uint8_t buf[PACKET_READ_BUF_SIZE];

    while (1) {
        // Чтение доступных байт из порта устройства
        memset(buf, 0, sizeof(buf));
        uint16_t bytesReaded = portIO->read(buf, sizeof(buf), USTA_PORT_READ_TIMEOUT);

        uint8_t packetLen = 0;

        if (bytesReaded <= 0) {
            return E_TIMEOUT;
        }

        // Обработка и удаление экранирующих символов
        for (size_t i = 0; i < bytesReaded; i++) {
            uint8_t byte = buf[i];

            if (response.size() == 0) {
                if (byte == PACKET_BYTE_START) {
                    response.push_back(byte);
                }

                continue;
            } else if (response.size() == 1) {
                packetLen = byte;
                response.push_back(byte);
                continue;
            } else {
                if (buf[i-1] == PACKET_BYTE_START) {
                    if (byte == PACKET_BYTE_ESC) {
                        continue;
                    } else {
                        response.clear();
                        response.push_back(PACKET_BYTE_START);
                        response.push_back(byte);
                        packetLen = byte;
                        continue;
                    }
                } else {
                    response.push_back(byte);
                }
            }
        }
/*
std::cerr << "Прочитанный packet: ";

for (size_t i = 0; i < response.size(); i++) {
    fprintf(stderr, "%.2X ", response.at(i));

}

std::cerr << std::endl;
*/
        // Проверка длины считанного пакета, дальнейшая обработка, если пакет считан полностью
        if (!response.empty() && (response.size() >= (packetLen + PACKET_OVERLENGHT))) {
            uint8_t checksum = 0;

            for (size_t i = 1; i < response.size(); i++) {
                checksum += response.at(i);
            }

//            std::cout << "Принятый пакет из УСТА (" << response.size() << "):" << response << "; CRC:" << checksum << std::endl;

            /*
            std::cerr << "Response packet (" << std::dec << response.size() << " bytes): ";

            for (size_t i = 0; i < response.size(); i++) {
                fprintf(stderr, "%.2X ", response.at(i));
            }

            std::cerr << std::endl;


            fprintf(stderr, "Checksum: %.2X\n", checksum);
            */

            if (checksum != 0) {
                return E_CHECKSUM_ERROR;
            }

            // Очистка от ненужных байт в начале и конце пакета

            // Delete first 2 overhead bytes
            response.erase(0, 2);

            // Delete checksum field
            response.resize(response.size() - 1);

            return E_OK;
        }

    }

    /*
     * На выходе из функции в переменную response записывается очищенный от экранирующих символов пакет данных, полученный от МСУ.
     * В нём удалены первые 2 байта (длина пакета и байт синхронизации) и байт контрольной суммы за ненадобностью.
     */

    return E_OK;
}

void USTADeviceDriver::constructRequestPacket(usta_device_request_data_t &packet, const byte_array &aluData, const fuel_data_record_t &fuelData) {
//    emb_time_struct_t timestruct;
//    uint32_t latitude, longitude;

    bzero(&packet, sizeof(packet));

    // Преобразование данных по топливу из внутреннего представления в формат посылки для УСТА

    // Заполнение структуры
    packet.command = 0x0E;
    packet.fuel_weight = fuelData.fuel_weight;
    packet.oil_temperature = aluData[20] << 8 | aluData[21];
    packet.water_temperature =  aluData[22] << 8 | aluData[23];
    packet.oil_pressure = aluData[4] << 8 | aluData[5] ;

    /*
    // Преобразование данных GPS из внутреннего предствления в формат посылки для УСТА

    // Set GPS data
    packet.gps_speed = gpsData.speed;
    packet.gps_speed *= 10; // TODO check this units

    //packet.gps_altitude = ;

    epoch_to_time(&timestruct, gpsData.time);
    packet.gps_timestamp_year = timestruct.year;
    packet.gps_timestamp_month = timestruct.month;
    packet.gps_timestamp_day = timestruct.day;
    packet.gps_timestamp_hour = timestruct.hour;
    packet.gps_timestamp_min = timestruct.min;
    packet.gps_timestamp_sec = timestruct.sec;

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_VALID) != 0)  {
        packet.gps_coord_status = USTA_PROTO_GPS_STATUS_3D;
    } else {
        packet.gps_coord_status = USTA_PROTO_GPS_STATUS_NONE;
    }

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_N) != 0) {
        packet.gps_coord_hs = USTA_PROTO_LAT_N;
    } else {
        packet.gps_coord_hs = USTA_PROTO_LAT_S;
    }

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_E) != 0) {
        packet.gps_coord_hs += USTA_PROTO_LONG_E;
    } else {
        packet.gps_coord_hs += USTA_PROTO_LONG_W;
    }

    latitude = longitude = 0;
    memcpy(&latitude, &gpsData.latitude, sizeof(gpsData.latitude));
    memcpy(&longitude, &gpsData.longitude, sizeof(gpsData.longitude));
    latitude <<= GPS_DATA_RECORD_COORD_SHIFT;
    longitude <<= GPS_DATA_RECORD_COORD_SHIFT;

    packet.gps_coord_lat_deg = latitude / GPS_DATA_RECORD_COORD_DEG_DIV;
    packet.gps_coord_long_deg = longitude / GPS_DATA_RECORD_COORD_DEG_DIV;

    latitude %= GPS_DATA_RECORD_COORD_DEG_DIV;
    longitude %= GPS_DATA_RECORD_COORD_DEG_DIV;

    packet.gps_coord_lat_min = latitude / GPS_DATA_RECORD_COORD_MIN_DIV;
    packet.gps_coord_long_min = longitude / GPS_DATA_RECORD_COORD_MIN_DIV;

    latitude %= GPS_DATA_RECORD_COORD_MIN_DIV;
    longitude %= GPS_DATA_RECORD_COORD_MIN_DIV;

    latitude *= 6;
    latitude /= 10;
    longitude *= 6;
    longitude /= 10;

    packet.gps_coord_lat_sec_int = latitude / GPS_DATA_RECORD_COORD_SEC_DIV;
    packet.gps_coord_long_sec_int = longitude / GPS_DATA_RECORD_COORD_SEC_DIV;

    latitude %= GPS_DATA_RECORD_COORD_SEC_DIV;
    longitude %= GPS_DATA_RECORD_COORD_SEC_DIV;

    packet.gps_coord_lat_sec_frac = latitude * 100;
    packet.gps_coord_long_sec_frac = longitude * 100;
    */
}

byte_array USTADeviceDriver::escapeBuffer(const byte_array &buf) {
    byte_array result;

    for (size_t i = 0; i < buf.size(); i++) {
        result.push_back(buf.at(i));

        if (buf.at(i) == USTA_PROTO_BYTE_START) {
            result.push_back(USTA_PROTO_BYTE_ESC);
        }
    }

    return result;
}

