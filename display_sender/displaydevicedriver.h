#ifndef DISPLAYDEVICEDRIVER_H
#define DISPLAYDEVICEDRIVER_H

#include <cstdint>
#include <string>

#include <typedefs.h>

#include <iserialportio.h>

//! Класс-драйвер для работы с ДМ
class DISPLAYDeviceDriver
{
public:
    //! Структура с описанием формата запроса данных от ДМ (содержит текущее состояние GPS и топливных датчиков)
    typedef struct __attribute__ ((__packed__)) {
        uint16_t            fuel_weight;
        int16_t             fuel_temperature;
        uint16_t            gps_coord_lat_sec_frac;
        uint16_t            gps_coord_long_sec_frac;
        uint16_t            gps_speed;
        int16_t             gps_altitude;
        uint8_t             gps_timestamp_year;
        uint8_t             gps_timestamp_month;
        uint8_t             gps_timestamp_day;
        uint8_t             gps_timestamp_hour;
        uint8_t             gps_timestamp_min;
        uint8_t             gps_timestamp_sec;
        uint8_t             gps_coord_lat_deg;
        uint8_t             gps_coord_lat_min;
        uint8_t             gps_coord_lat_sec_int;
        uint8_t             gps_coord_long_deg;
        uint8_t             gps_coord_long_min;
        uint8_t             gps_coord_long_sec_int;
        uint8_t             gps_coord_hs;
        uint8_t             gps_coord_status;
    } display_device_request_data_t;

    //! Перечисление значений возможных служебных байт
    enum DISPLAY_PROTO_BYTES : uint8_t {
        DISPLAY_PROTO_BYTE_ESC          =   0xFE,   /*!< Экранирующий символ */
        DISPLAY_PROTO_BYTE_START        =   0xFF    /*!< Байт синхронизации */
    };

    //! Перечисление возможных состояний GPS
    enum DISPLAY_PROTO_GPS_STATUS : uint8_t {
        DISPLAY_PROTO_GPS_STATUS_NONE   =   0x00,
        DISPLAY_PROTO_GPS_STATUS_2D     =   0x01,
        DISPLAY_PROTO_GPS_STATUS_3D     =   0x02
    };

    DISPLAYDeviceDriver();

    ~DISPLAYDeviceDriver();

    //! Открыть порт ДМ
    ERROR_CODE openPort(std::string port, uint32_t speed);

    //! Закрыть порт устройства
    void closePort() {
        portIO->close();
    }

    /*!
     * \brief Отправка запроса с текущим состоянием системы (GPS и топливо).
     * \param gpsData текущие данные GPS
     * \param fuelData текущие данные по топливу
     * \return код ошибки
     */
    ERROR_CODE sendDataRequest(const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData);

protected:

    //! Функция формирования посылки для ДМ с указанием текущих значений GPS и топлива
    void constructRequestPacket(display_device_request_data_t &packet, const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData);

    //! Добавление экранирующих байт
    byte_array escapeBuffer(const byte_array &buf);

private:
    //! Указатель на экземпляр класса для работы с последовательным портом
    ISerialPortIO *portIO;
};

#endif // DISPLAYDEVICEDRIVER_H
