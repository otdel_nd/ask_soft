#include "displaydevicedriver.h"

#include <iostream>
#include <cassert>
#include <cstring>

#include <elogger.h>

#include <serialportiofabric.h>

#define DISPLAY_PORT_INTERBYTE_TIMEOUT  SERIALPORTIO_CONVERT_MS(100)    // 100mS
#define DISPLAY_PORT_READ_TIMEOUT       SERIALPORTIO_CONVERT_MS(500)    // 500mS
#define DISPLAY_PROTO_CHECKSUM_LEN      1

#define DISPLAY_PROTO_LAT_N             10
#define DISPLAY_PROTO_LAT_S             20
#define DISPLAY_PROTO_LONG_E            1
#define DISPLAY_PROTO_LONG_W            2

#define PACKET_READ_BUF_SIZE        512

#define PACKET_BYTE_START           0xFF
#define PACKET_BYTE_ESC             0xFE
#define PACKET_FIXED_PAYLOAD_LEN    201
#define PACKET_FIXED_FULL_LEN       203
#define PACKET_OVERLENGHT           2U

DISPLAYDeviceDriver::DISPLAYDeviceDriver() :
    portIO(NULL)
{
    portIO = SerialPortIOFabric::getInstance();
    assert(portIO != NULL);
}

DISPLAYDeviceDriver::~DISPLAYDeviceDriver() {
    if (portIO != NULL) {
        portIO->close();
        delete portIO;
    }
}


ERROR_CODE DISPLAYDeviceDriver::openPort(std::string port, uint32_t speed) {
    int res;

    assert(portIO != NULL);

    // Установка скорости и других параметров последовательного порта
    portIO->setSpeed(speed);
    portIO->setFlowControl(ISerialPortIO::FLOW_OFF);
    portIO->setParity(ISerialPortIO::PAR_EVEN);
    portIO->setInterByteTimeout(DISPLAY_PORT_INTERBYTE_TIMEOUT);
    portIO->setReadTimeout(DISPLAY_PORT_READ_TIMEOUT);

    // Непосредственно открытие порта
    res = portIO->open(port.c_str());

    if (res >= 0) {
        // Применение настроек порта
        portIO->setSettings();
    } else {
        return E_FAIL;
    }

    return E_OK;
}

ERROR_CODE DISPLAYDeviceDriver::sendDataRequest(const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData) {
    display_device_request_data_t data;
    byte_array packet;              // Байтовый буфер данных для отправки в ДМ
    uint8_t checksum = 0;
    ssize_t bytesWritten;
//    int bytesWritten;

    // Формирование пакета для отправки данных в ДМ
    constructRequestPacket(data, gpsData, fuelData);

    // Добавление байта длины пакета в буфер данных для отправки
    packet.push_back(sizeof(data) + DISPLAY_PROTO_CHECKSUM_LEN);

    // Добавление данных тела пакета в буфер для отправки
    packet.append(reinterpret_cast<const uint8_t *>(&data), sizeof(data));

    // Вычисление контрольной суммы
    for (size_t i = 0; i < packet.size(); i++) {
        checksum += packet.at(i);
    }

    checksum -= 0xFF;
    checksum = (int8_t)((~checksum)+1) ;
    checksum++;

    // Добавление контрольной суммы в буфер для отправки
    packet.push_back(checksum) ;

    // Добавление экранирующих символов в буфер для отправки
    packet = escapeBuffer(packet);

    // Добавление байта синхронизации в начало пакета
    packet.insert(0, 1, DISPLAY_PROTO_BYTE_START);

    ELOG(ELogger::INFO_DEVICE, ELogger::LEVEL_TRACE) << "Отправлен пакет в ДМ (" << packet.size() << "):" << packet;

    /*
    std::cerr << "Writing packet: ";
    for (size_t i = 0; i < packet.size(); i++) {
        fprintf(stderr, "%.2X ", packet.at(i));
    }   
    std::cerr << std::endl;
    */


    // Запись сформированного буфера в порт
    bytesWritten = portIO->write(packet);

    if ((bytesWritten >= 0) && (bytesWritten == (int)packet.size())) {
        return E_OK;
    } else {
        return E_WRITE_ERROR;
    }
}

void DISPLAYDeviceDriver::constructRequestPacket(display_device_request_data_t &packet, const gps_data_record_t &gpsData, const fuel_data_record_t &fuelData) {
    emb_time_struct_t timestruct;
    uint32_t latitude, longitude;

    bzero(&packet, sizeof(packet));

    // Преобразование данных по топливу из внутреннего представления в формат посылки для ДМ

    // Set fuel data
    packet.fuel_weight = fuelData.fuel_weight;
    packet.fuel_temperature = fuelData.fuel_temperature;

    // Преобразование данных GPS из внутреннего предствления в формат посылки для ДМ

    // Set GPS data
    packet.gps_speed = gpsData.speed;
    packet.gps_speed *= 10; // TODO check this units

    //packet.gps_altitude = ;

    epoch_to_time(&timestruct, gpsData.time);
    packet.gps_timestamp_year = timestruct.year;
    packet.gps_timestamp_month = timestruct.month;
    packet.gps_timestamp_day = timestruct.day;
    packet.gps_timestamp_hour = timestruct.hour;
    packet.gps_timestamp_min = timestruct.min;
    packet.gps_timestamp_sec = timestruct.sec;

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_VALID) != 0)  {
        packet.gps_coord_status = DISPLAY_PROTO_GPS_STATUS_3D;
    } else {
        packet.gps_coord_status = DISPLAY_PROTO_GPS_STATUS_NONE;
    }

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_N) != 0) {
        packet.gps_coord_hs = DISPLAY_PROTO_LAT_N;
    } else {
        packet.gps_coord_hs = DISPLAY_PROTO_LAT_S;
    }

    if ((gpsData.flags & GPS_DATA_RECORD_FLAGS_E) != 0) {
        packet.gps_coord_hs += DISPLAY_PROTO_LONG_E;
    } else {
        packet.gps_coord_hs += DISPLAY_PROTO_LONG_W;
    }

    latitude = longitude = 0;
    memcpy(&latitude, &gpsData.latitude, sizeof(gpsData.latitude));
    memcpy(&longitude, &gpsData.longitude, sizeof(gpsData.longitude));
    latitude <<= GPS_DATA_RECORD_COORD_SHIFT;
    longitude <<= GPS_DATA_RECORD_COORD_SHIFT;

    packet.gps_coord_lat_deg = latitude / GPS_DATA_RECORD_COORD_DEG_DIV;
    packet.gps_coord_long_deg = longitude / GPS_DATA_RECORD_COORD_DEG_DIV;

    latitude %= GPS_DATA_RECORD_COORD_DEG_DIV;
    longitude %= GPS_DATA_RECORD_COORD_DEG_DIV;

    packet.gps_coord_lat_min = latitude / GPS_DATA_RECORD_COORD_MIN_DIV;
    packet.gps_coord_long_min = longitude / GPS_DATA_RECORD_COORD_MIN_DIV;

    latitude %= GPS_DATA_RECORD_COORD_MIN_DIV;
    longitude %= GPS_DATA_RECORD_COORD_MIN_DIV;

    latitude *= 6;
    latitude /= 10;
    longitude *= 6;
    longitude /= 10;

    packet.gps_coord_lat_sec_int = latitude / GPS_DATA_RECORD_COORD_SEC_DIV;
    packet.gps_coord_long_sec_int = longitude / GPS_DATA_RECORD_COORD_SEC_DIV;

    latitude %= GPS_DATA_RECORD_COORD_SEC_DIV;
    longitude %= GPS_DATA_RECORD_COORD_SEC_DIV;

    packet.gps_coord_lat_sec_frac = latitude * 100;
    packet.gps_coord_long_sec_frac = longitude * 100;
}

byte_array DISPLAYDeviceDriver::escapeBuffer(const byte_array &buf) {
    byte_array result;

    for (size_t i = 0; i < buf.size(); i++) {
        result.push_back(buf.at(i));

        if (buf.at(i) == DISPLAY_PROTO_BYTE_START) {
            result.push_back(DISPLAY_PROTO_BYTE_ESC);
        }
    }

    return result;
}

