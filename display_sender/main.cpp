#include <iostream>
#include <cassert>
#include <getopt.h>
#include <cstring>

#include <elogger.h>
#include <daemon.h>

#include <inireader.h>
#include <displaypollcontroller.h>

using namespace std;
//! Параметры, задаваемые из командной строки
struct globalArgs_t {
    uint32_t                device_speed;           // -s
    std::string             device_path;            // -d
    int                     timeout;                // -t
    std::string             configPath;             // --config
    std::string             pidFilePath;            // --pidfile
} globalArgs;

//! Описание допустимых опций командной строки
static const char *opts_string = "s:d:t:bh";

//! Описание допустимых опций командной строки
static const struct option long_opts[] = {
    { "speed",      required_argument,  NULL,   's' },
    { "dev",        required_argument,  NULL,   'd' },
    { "timeout",    required_argument,  NULL,   't' },
    { "config",     required_argument,  NULL,   0 },
    { "pidfile",    required_argument,  NULL,   0 },
    { "daemonize",  no_argument,        NULL,   'b' },
    { "help",       no_argument,        NULL,   'h' },
    { NULL,         no_argument,        NULL,   0 }
};

void print_help() {
    printf("Это страница помощи для работы с программой display_sender\n"
           "Список опций:\n"
           "    -h          --help              отображает то, что вы видите сейчас\n"
           "    -b          --daemonize         запуск в режиме демона\n"
           "    -s speed    --speed speed       установить скорость обмена (по умолчанию 115200)\n"
           "    -d device   --dev device        установить путь к устройству (по умолчанию /dev/ttySU1)\n"
           "    -t timeout  --timeout timeout   установить периодичность опроса устройства\n"
           "                --config file_path  установить путь к config файлу (default /etc/ask/ask.conf)\n"
           "                --pidfile file_path установить путь к pid файлу (default /tmp/run/dm.pid)\n"
           );
}

DISPLAYPollController *pollController = NULL;

void initLogger() {
    ELogger::initLogger();

    ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_INFO) << "display_sender запущен";
}

int main(int argc, char *argv[]) {
    int opt = 0;
    int option_index;

    // Задание значений опций по умолчанию
    globalArgs.device_path = "/dev/ttySU1";
    globalArgs.device_speed = 115200;
    globalArgs.timeout = 1;
    globalArgs.configPath = "/etc/ask/ask.conf";
    globalArgs.pidFilePath = "/tmp/run/dm.pid";

    // Обработка опций командной строки
    opt = getopt_long(argc, argv, opts_string, long_opts, &option_index);
    while (opt != -1) {
        switch( opt ) {
        case 's':
            globalArgs.device_speed = atoi(optarg);
            break;

        case 'd':
            globalArgs.device_path = optarg;
            break;

        case 't':
            globalArgs.timeout = atoi(optarg);
            if (globalArgs.timeout < 0) {
                globalArgs.timeout = 0;
            }
            break;

        case 'h':
            print_help();
            return 0;

        case 'b':
            Daemon::daemonize();
            break;

        case 0:
            if (strcmp("config", long_opts[option_index].name) == 0) {
                globalArgs.configPath = optarg;
            }

            if (strcmp("pidfile", long_opts[option_index].name) == 0) {
                globalArgs.pidFilePath = optarg;
            }
            break;

        default:
            /* сюда попасть невозможно. */
            break;
        }

        opt = getopt_long(argc, argv, opts_string, long_opts, &option_index);
    }

    initLogger();

    INIReader config(globalArgs.configPath);

    if (config.parseResult() == 0) {
        // Читаем файл конфигурации
        globalArgs.device_path =            config.get("dm", "dev_path", globalArgs.device_path);
        globalArgs.device_speed =           config.getUInt("dm", "dev_speed", globalArgs.device_speed);
        globalArgs.timeout =                config.getUInt("dm", "dev_timeout", globalArgs.timeout);
        globalArgs.pidFilePath =            config.get("dm", "pidfile", globalArgs.pidFilePath);
    } else {
        ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Config file parse error:" << config.parseResult();
    }

    Daemon::checkInstance(globalArgs.pidFilePath);
    uint8_t res;

    // Создание экземпляра контроллера опроса ДМ
    pollController = new DISPLAYPollController();
    assert(pollController != NULL);

    // Открытие порта устройства ДМ
    res = pollController->openPort(globalArgs.device_path, globalArgs.device_speed);
    if (res != E_OK) {
        ELOG(ELogger::INFO_DEVICE, ELogger::LEVEL_ERROR) << "Не могу открыть порт " << globalArgs.device_path << "; Ошибка: " << res;
        delete pollController;
        return 1;
    }

    // Запуск опроса ДМ. Функция передаёт управление дальше только при выходе из программы
    pollController->start(globalArgs.timeout);

    // Очистка ресурсов
    if (pollController != NULL) {
        delete pollController;
    }

    return 0;
}
