#ifndef DISPLAYPOLLCONTROLLER_H
#define DISPLAYPOLLCONTROLLER_H

#include <atomic>

#include <typedefs.h>
#include <shmstorage.h>
#include <displaydevicedriver.h>

//! Класс контроллера опроса ДМ
class DISPLAYPollController
{
public:
    /*!
     * \brief Конструктор класса
     * \param
     * \param
     */
    DISPLAYPollController();

    ~DISPLAYPollController();

    /*!
     * \brief функция открытия порта для работы с МСУ
     * \param port путь к файлу устройства
     * \param speed скорость работы порта
     * \return код ошибки
     */
    ERROR_CODE openPort(std::string port, uint32_t speed);

    //! Функция опроса ДМ
    /*! Опрос выполняется в вызывающем потоке. Функция блокируется до вызова функции stop из другого потока */
    void start(int timeout);

    //! Прервать цикл опроса ДМ
    void stop() { isRunning = false; }


private:
    //! Указатель на экземпляр класса-драйвера ДМ
    DISPLAYDeviceDriver *device;

    //! Экземпляр класса-хранилища (в разделяемой между приложениями памяти) для получения текущий данных по топливу
    /*!
        Данный класс исользуется для получения данных от приложения работы с топливными датчиками. Данные нужны для отправки в МСУ.
        Получение данных идёт при помощи разделяемой памяти (SHM).
     */
    SHMStorage<fuel_data_record_t> fuelSharedData;
    //! Экземпляр класса-хранилища (в разделяемой между приложениями памяти) для получения данных по текущему состоянию GPS
    /*!
        Данный класс исользуется для получения данных от приложения работы с GPS. Данные нужны для отправки в МСУ.
        Получение данных идёт при помощи разделяемой памяти (SHM).
     */
    SHMStorage<gps_data_record_t> gpsSharedData;

    std::atomic<bool> isRunning;
};

#endif // DISPLAYPOLLCONTROLLER_H
