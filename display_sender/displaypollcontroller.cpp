#include <cassert>
#include <iostream>

#include "displaypollcontroller.h"

DISPLAYPollController::DISPLAYPollController() :
    device(NULL), fuelSharedData("fuel_data"),
    gpsSharedData("gps_data"),
    isRunning(false)
{
    // Создание экземпляра класса-драйвера МСУ
    device = new DISPLAYDeviceDriver();
    assert(device != NULL);
}

DISPLAYPollController::~DISPLAYPollController() {
    isRunning = false;

    if (device != NULL) {
        device->closePort();
        delete device;
    }
}

ERROR_CODE DISPLAYPollController::openPort(std::string port, uint32_t speed) {
    return device->openPort(port, speed);
}

void DISPLAYPollController::start(int timeout) {
    uint8_t res;
    fuel_data_record_t fuelData;
    gps_data_record_t gpsData;
    byte_array displayData;

    isRunning = true;

    // Опрос ДМ пока не будет вызван метод stop()
    /*
     * Данные о текущих показаниях топливного датчика и GPS считываются и отправляются в виде запроса на ДМ.
     * После чего считывается ответ от ДМ и формируется структура с ответом ДМ, временем получения сообщения, итп.
     * Далее ответ от ДМ должен формироваться в строку для отправки на сайт.
     */
    while (isRunning) {
        // Задержка между опросами
        sleep(timeout);

        // Получение текущих значений от программ работы с GPS и топливными датчиками посредством разделяемой памяти
        gpsData = gpsSharedData.get();
        fuelData = fuelSharedData.get();

        // Отправка запроса на ДМ с данными о текущем состоянии GPS и топливных датчиков
        res = device->sendDataRequest(gpsData, fuelData);
        if (res != E_OK) {
            ELOG(ELogger::INFO_SYSTEM, ELogger::LEVEL_WARN) << "Не удалось отправить запрос на ДМ. Ошибка: " << res ;

            continue;
        }

        // Очистка переменной с данными перед чтением ответа
        displayData.clear();
     }

}
