#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <write_ask_file.h>
#include <ipx_server.h>
#include <typedefs.h>

FILE *fask ;										// указатель на файл .ask
void make_file_name( char * ) ;								// функция формирует имя файла
unsigned char ask_data[150] ;								// массив содержащий текущие АСК данные
char file_name[50] ;									// строка с именем файла текущего .ask файла (нужно больше)

extern data_message_t data_message ;

int write_ask_file( int zer_size, int *count_record, char *file_name )
{

	int i ;
	uint16_t FF0F = 0x0FFF ;							// признак начала зеркала
/*
	print_log( "Размер зеркала: %d\n", zer_size ) ;
        for ( i=0 ; i<zer_size ; i++ ) {
                print_log( "%4d: %X", i, zerkalo[i] ) ;
		if(i%10==0) print_log("\n------------------------------------------------------------------------------\n") ;
	}
        print_log( "\n" ) ; 
*/	
	if ( *count_record == 0 )							// если предыдущее количество записей равно 0 (то есть
	{										// предыдущий файл переполнен или не существует)
		memset( file_name, '\0', 50 ) ;						// обнулим массив содержащий строку с именем файла
		make_file_name( file_name ) ;						// формируем имя файла

		if (( fask = fopen( file_name, "wb" )) == NULL)				// то создадим файл с этим именем
		{
			print_log( "Ошибка открытия файла %s", file_name ) ;
			return -1 ;
		}

		print_log( "Создан файл с именем: %s\n", file_name ) ;
		fwrite( &FF0F, sizeof(unsigned char), 2, fask ) ;			// начало записи
		fwrite( zerkalo, sizeof(unsigned char), zer_size, fask ) ;		// запишем в файл зеркало
		int ask_data_size = get_ask_data( ask_data ) ;				// получим АСК данные
		fwrite( ask_data, sizeof(unsigned char), ask_data_size, fask ) ;	// запишем в файл АСК данные
		fwrite( data_message.msg_text, sizeof(unsigned char), 59, fask ) ;
		(*count_record)++ ;							// увеличим число записей
		fclose(fask) ;								// закроем файл
	}
	else										// если файл уже существует 
	{										//(то есть число записей не равно нулю)
		if ( *count_record < 599 )						// и при этом число записей не достигло максимального
		{
			if ((fask = fopen( file_name, "rb+" )) == NULL)			// откроем файл для записи (в file_name
			{								// должно быть текущее имя)
				print_log( "Не удалось открыть файл %s для чтения-записи", file_name ) ;
				return -1 ;
			}
			fseek( fask, 0, SEEK_END ) ;					// перед записью в файл переместимся в конец файла
			fwrite( &FF0F, sizeof(unsigned char), 2, fask ) ;		// начало записи
			fwrite( zerkalo, sizeof(unsigned char), zer_size, fask ) ;	// запишем в файл зеркало
			int ask_data_size = get_ask_data( ask_data ) ;			// получим АСК данные
			fwrite( ask_data, sizeof(unsigned char), ask_data_size, fask ) ;// запишем в файл АСК данные
			fwrite( data_message.msg_text, sizeof(unsigned char), 59, fask ) ;
			(*count_record)++ ;						// увеличим число записей
			fclose(fask) ;							// закроем файл
		}
		else									// если число записей равно максимальному значению-1 (600-1)
		{
			if ((fask = fopen( file_name, "rb+" )) == NULL)			// откроем файл для записи (в file_name
			{								// должно быть текущее имя)
				print_log("Не удалось открыть файл %s для чтения-записи", file_name) ;
				return -1 ;
			}
			fseek( fask, 0, SEEK_END ) ;					// перед записью в файл переместимся в конец файла
			fwrite( &FF0F, sizeof(unsigned char), 2, fask ) ;		// начало записи
			fwrite( zerkalo, sizeof(unsigned char), zer_size, fask) ;	// запишем последнее зеркало в файл
			int ask_data_size = get_ask_data( ask_data ) ;			// получим АСК данные
			fwrite( ask_data, sizeof(unsigned char), ask_data_size, fask ) ;// запишем в файл АСК данные
			fwrite( data_message.msg_text, sizeof(unsigned char), 59, fask ) ;
			*count_record = 0 ;						// обнулим число записей
			fclose(fask) ;							// закроем файл
		}
	}
	return 0 ;									// успешно
}

void make_file_name( char * file_name )
{
	char tmp[50] ;

	strcat(file_name, "/mnt/sdcard/ASK_FILES/WORK/") ;					// путь к папке ASK_FILES

	switch ((int)loc_num/100000)							// добавим в имя файла тип тепловоза
	{
		case 510 : strcat( file_name, "TEP70BS_" ) ; break ;
		case 606 : strcat( file_name, "2TE116_") ; break ;
		case 608 : strcat( file_name, "2TE25A_") ; break ;
		case 696 : strcat( file_name, "2TE25KM_") ; break ;
	}
	
	sprintf(tmp, "%d", loc_num) ;
	strcat( file_name, tmp ) ;							// добавим в имя файла номер loc_num (из config-файла)

	strcat( file_name, "_" ) ;	

	get_time() ;
	strftime( tmp, 25, "%d%m%H%M", ptime ) ;
	strcat( file_name, tmp ) ;							// а также время и дату сбора данных

	strcat( file_name, ".ask" ) ;
}
