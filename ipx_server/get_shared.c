#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include <shmstorage.h>
#include <typedefs.h>
#include <print_log.h>
#include <ipx_server.h>

FILE *fconfig ;												// указатель на файл с конфигурацией АСК
int loc_num ;												// номер локомотива из config файла
int l_num ;												// порядковый номер локомотива
int id ;

extern SHMStorage<gps_data_record_t>* gpsSharedData ;
extern SHMStorage<fuel_data_record_t>* fuelSharedData ;

int get_gps_data(int* c1, int* c2, double* c3, int* c4, int* c5, double* c6, int* speed)
{
	double rez ;

	gps_data_record_t record ;									// структура в которой будет храниться текущее значение координат (описание в typedefs.h)
	record = gpsSharedData->get() ;									// извлечем данные по GPS из разделяемой памяти с помощью метода get() класса SHMStorage

	rez = (record.longitude[2] << 16 | record.longitude[1] << 8 | record.longitude[0]) << 3 ;	// выясняем долготу, rez содержит число типа XXXX.XXXX
	*c4 = (int)(rez/1000000) ;
	*c5 = (int)((rez-(((int)(rez/1000000))*1000000))/10000) ;
	*c6 = ((((rez/10000)-((int)(rez/10000)))*100000)*60)/100000 ;

	rez = (record.latitude[2] << 16 | record.latitude[1] << 8 | record.latitude[0]) << 3 ;		// выясняем широту, rez содержит число типа XXXX.XXXX
	*c1 = (int)(rez/1000000) ;
	*c2 = (int)((rez-(((int)(rez/1000000))*1000000))/10000) ;
	*c3= ((((rez/10000)-((int)(rez/10000)))*100000)*60)/100000 ;

	*speed = (int)record.speed ;

	return 0 ;
}

int get_fuel_data(int* fuel)
{
	fuel_data_record_t record ;			// структура в которой будет хранится текущее значение топлива (описание в typedefs.h)
	record = fuelSharedData->get() ;		// заполним структуру с помощью метода get() класса SHMStorage
	*fuel = record.fuel_weight ;			// вернем окружению только интересующий параметр
	return 0 ;
}


void get_config( void )								// функция читает данные из файла конфигурации
{
	char lstr[9] ;								// для извлечения номера локомотива
	char num3[1] ;
        char num[3] ;
	char tmp[25] ;								// временный массив
	if ((fconfig = fopen(PATH_CONFIG, "r")) == NULL)
	{
		print_log("Не удалось открыть файл конфигурации.") ;
	}
	else
	{
		while(!fscanf(fconfig, "serial = %s", tmp))			// пока в config-файле не встретится строка serial = "
		{
			fgetc(fconfig) ;					// увеличиваем указатель текущей позиции файла
		}

		if(tmp[7]=='0')
			loc_num = atoi(tmp) ;					// считываем номер локомотива
		else
			loc_num = atoi(tmp)-1 ;
        }
	fclose(fconfig) ;							// закрываем файл конфигурации АСК
	
        snprintf(lstr, 9, "%d", loc_num) ;   		                        // извлечем из loc_num только номер тепловоза
        num[0] = lstr[4] ; num[1] = lstr[5] ; num[2] = lstr[6] ; num3[0] = lstr[7] ;
        l_num = atoi(num) ;                                    	               	// сохраним
	
	id = (l_num>>8)+(uint8_t)l_num + atoi(num3) ;
}

// функция извлекает АСК данные из системы и сохраняет их в массив (для последующей записи в файл)
int get_ask_data( unsigned char *ask_data )
{
	typedef struct __attribute__((__packed__))	// в этой структуре данные хранятся в том порядке, в каком их нужно записать в файл
	{
		// 24 бита (значение XXXX.XXXX >> 3)
		uint8_t latitude[3] ;
		// 24 бита (значение XXXX.XXXX >> 3)
		uint8_t longitude[3] ;
		uint8_t speed;
		float32_t   left_sensor_raw ;
		float32_t   right_sensor_raw ;
		uint16_t    fuel_weight ;
		int8_t      fuel_temperature ;
		float32_t   board_temp ;
		uint8_t year ;
		uint8_t month ;
		uint8_t day ;
		uint8_t hour ;				// московское время
		uint8_t min ;
		uint8_t sec ;
	} ask_data_packed ;				// также сюда нужно добавить дискретные вх/вых, темп. проц. и другое

	emb_time_struct_t* emb_time ;			// указатель на cтруктуру времени составного типа

	fuel_data_record_t record_fuel ;		// структура для хранения данных по топливу
	record_fuel = fuelSharedData->get() ;		// заполним структуру record_fuel

	gps_data_record_t record_gps ;			// структура для хранения данных по GPS
	record_gps = gpsSharedData->get() ;		// заполним структуру record_gps

	emb_time = epoch_to_time(emb_time, record_gps.time );

	ask_data_packed *packed_data = (ask_data_packed *)ask_data ;
	packed_data->latitude[0] = record_gps.latitude[0] ;
	packed_data->latitude[1] = record_gps.latitude[1] ;
	packed_data->latitude[2] = record_gps.latitude[2] ;
	packed_data->longitude[0] = record_gps.longitude[0] ;
	packed_data->longitude[1] = record_gps.longitude[1] ;
	packed_data->longitude[2] = record_gps.longitude[2] ;
	packed_data->speed = record_gps.speed ;
	packed_data->left_sensor_raw = record_fuel.left_sensor_raw ;
	packed_data->right_sensor_raw = record_fuel.right_sensor_raw ;
	packed_data->fuel_weight = record_fuel.fuel_weight ;
	packed_data->fuel_temperature = record_fuel.fuel_temperature ;
	packed_data->board_temp = (float32_t)get_board_temp() ;
	packed_data->year = emb_time->year ;
	packed_data->month = emb_time->month ;
	packed_data->day = emb_time->day ;
	packed_data->hour = (emb_time->hour)+(uint8_t)4 ;	// Московское время
	packed_data->min = emb_time->min ;
	packed_data->sec = emb_time->sec ;
	
/*	printf("\nРазмер массива ask_data = %d\n", sizeof(ask_data_packed)) ;
        for ( int i=0 ; i<sizeof(ask_data_packed) ; i++ )
                printf( "%X ", ask_data[i] ) ;
        printf( "\n" ) ;
*/
	return sizeof(ask_data_packed) ;		// вернем количество элементов массива ask_data
}

float get_board_temp( void )
{
	int i2c_file ;
	int addr = 0x49 ; 				/* The I2C address of TMP102 */
	char buf[2] = {0} ;
	int temp ;
	unsigned char MSB, LSB ;
	float f, c ;

	if ((i2c_file = open("/dev/i2c-1", O_RDWR)) < 0)
	{
		print_log("Не удалось открыть i2c устройство") ;
		return -300 ;
	}

	if (ioctl(i2c_file, I2C_SLAVE, addr) < 0)
	{
		print_log("Не удалось зауправлять i2c устройством") ;
		return -300 ;
	}

	if (read(i2c_file,buf,2) != 2)
	{
		print_log("Не удалось прочитать из i2c устройства") ;
		return -300 ;
	}
	else
	{
		MSB = buf[0];
		LSB = buf[1];
		temp = ((MSB << 8) | LSB) >> 4;
		c = temp*0.0625;
		f = (1.8 * c) + 32;
	}
	printf("Temp Fahrenheit: %f Celsius: %f\n", f, c);
	close( i2c_file ) ;
	return c ;
}
