#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>

#include <ipx_server.h>
#include <write_ask_file.h>
#include <shmstorage.h>
#include <typedefs.h>

unsigned char* zerkalo ;									// динамический массив для хранения зеркала
SHMStorage<gps_data_record_t>* gpsSharedData ;							// указатель на объект класса SHMstorage для работы с разделяемой памятью
SHMStorage<fuel_data_record_t>* fuelSharedData ;						// указатель на объект класса SHMStorage для работы с разделяемой памятью
SHMStorage<ipx_data_data_t>* ipxSharedData ;							// указатель на объект класса SHMStorage для работы с разделяемой памятью
SHMStorage<ipx_data_visit_t>* ipxSharedVisit ;							// указатель на объект класса SHMStorage для работы с разделяемой памятью
SHMStorage<post_data_t>* postSharedData ;							// указатель на объект класса SHMStorage для работы с разделяемой памятью

int ipx_flag = 1 ;
int die_flag = 1 ;
pid_t IPXServer,IPXChecker ;									// идентификаторы процессов
struct timespec twr, tw = {0,500000000} ;

data_message_t data_message ;

int ipx_server( void )
{
	struct sigaction sa ;									// структура для управления сигналами
	sigemptyset(&sa.sa_mask) ;								// очищаем маску сигналов (не текущего процесса, а структуры sa)
	sa.sa_handler = catchsig ;								// указатель на функцию-обработчик сигналов
	sa.sa_flags = 0|SA_NODEFER ;	
	sigaction(SIGTERM, &sa, NULL) ;								// для корректного завершения
		
 	get_config( ) ;										// прочитаем данные из файла конфигурации (после этого в l_num поряд. номер тепловоза и т.д.)
	set_pid_file( "ipx_server_pid" ) ;							// сохраним идентификатор группы в файл (для завершения)
	
	IPXServer = getpid() ;
		
	pid_t pid = fork() ;

	if (pid == 0)
	{											// дочерний процесс
		if( ipx_daemon() == -1 )							// получает IPX пакеты и сохраняет их в разделяемой памяти
		{
			print_log( "Не удалось запустить IPX-демон.\nНужно выполнить: ipx_interface add -p eth0 802.2 0x0000029a" ) ;
			exit(1) ;	// кроме того нужно послать сигнал родительскому процессу о завершении
		}
	}

	if (pid > 0)										// родительский процесс
	{
	int count_record = 0 ;									// текущее количество записей в .ask файле (изначально равно нулю)
	postSharedData = new SHMStorage<post_data_t>("post_data") ;				// выделим память под объекты класса SHMStorage
	ipxSharedData = new SHMStorage<ipx_data_data_t>("ipx_data") ;				// 
	ipxSharedVisit = new SHMStorage<ipx_data_visit_t>("ipx_visit") ;			// 
	gpsSharedData = new SHMStorage<gps_data_record_t>("gps_data") ;				// 
	fuelSharedData = new SHMStorage<fuel_data_record_t>("fuel_data") ;			// 
	ipx_data_visit_t ipx_data_visit ;							// для хранения данных принятой визитки

	sigaction(SIGUSR1, &sa, NULL) ;								// устанавливает новую диспозицию для сигнала SIGUSR1
	sigaction(SIGUSR2, &sa, NULL) ;								// устанавливает новую диспозицию для сигнала SIGUSR2

	sleep(3) ;										// подождем, вдруг придет придет сигнал об отсутсвии IPX-пакетов
	while(die_flag)										// пока не поступило сигнала завершить программу
		{
		nanosleep(&tw, &twr) ;								// спать 500 мс
		while( nanosleep(&twr, &twr)==-1 && errno == EINTR )				// сон прервется сигналом, поэтому досыпаем
			continue ;

			if(ipx_flag)								// если нужные ipx-пакеты приходят по сети 
			{
printf("\nПакеты приходят\n") ; get_board_temp( ) ;
				ipx_data_visit = ipxSharedVisit->get() ;			// получаем данные визитки
				if( save_zer( ipx_data_visit.zer_size, ipx_data_visit.zer_block_size, ipx_data_visit.upravl_size ) == -1 ) 		// сохраняем текущее зеркало в динамический массив *zerkalo
					continue ;						// прервем итерацию, если save_zer() вернула -1
				get_post_data( IPX_PACKET ) ;					// извлекаем из зеркала текущие данные и пакуем в POST массив
				write_ask_file( ipx_data_visit.zer_size, &count_record, file_name ) ;	// пишем текущее зеркало в файл
				free( zerkalo ) ;						// освобождаем память
			} // конец блока, когда IPX пакеты приходят
			else									// если нужные ipx-пакеты перестали приходить 
			{	
printf("\nПакеты не приходят\n") ;
				zerkalo = new unsigned char [400] ;				// память для зеркала
				for(int i = 0; i<400; i++)
					zerkalo[i] = 0 ;					// инициализируем зеркало нулевыми значениями
				get_post_data( NO_IPX_PACKET ) ;				// извлекаем из зеркала текущие данные и пакуем в POST массив
				write_ask_file( 400, &count_record, file_name ) ;
				delete [] zerkalo ; 						// освобождаем память
			} // конец блока, когда нужные ipx-пакеты перестали приходить
		} // конец бесконечного цикла
	printf("\nПроцесс %d сдох\n", getpid() ) ;	
	} // родительский процесс
} //ipx_server( void )


int save_zer( int zer_size, int zer_block_size, int upravl_size )
{
	int num_zerk = 1000 ;														// для хранения номера текущего зеркала
	int i,t,j ;
	int part_count ;														// для хранения числа частей
	if (zer_block_size == 0)													// если по какой-то причине размер части блока равен 0
		part_count = 1 ;													// считаем что зеркало состоит из одной части
	else																// всегда же
		part_count = zer_size / zer_block_size ; 										// выясним в скольких пакетах помещается наше зеркало
//	part_count = 4 ;

	if(( zerkalo = (unsigned char*)calloc(zer_size, sizeof(unsigned char) )) == NULL)
	{
		print_log("Ошибка при распределении памяти") ;
		exit(1) ;
	}

	while ( die_flag == 1 && ipx_flag == 1 && ((num_zerk = receiv_ipx_data( upravl_size, FIRST_ZER_BLOCK, part_count )) == -1) )							// получаем пакет с данными
		continue ;
	
	if(num_zerk == -1)														
		return -1 ;

	printf( "\nНомер зеркала %d \n", num_zerk ) ;

	for ( i=0; i<zer_block_size; i++ )
		zerkalo[i] = data_buf[i+11+upravl_size] ;										// сохраним первую часть зеркала
	part_count-- ;															// уменьшим на 1 число частей

	while ( part_count != 0)													// пока части не собраны
	{

		while ( die_flag == 1 && ipx_flag == 1 && ((num_zerk = receiv_ipx_data( upravl_size, NEXT_ZER_BLOCK, part_count )) == -1) )		// получаем IPX пакет
			continue ;													// с тем же номером зеркала;

		t = i ;
		for ( i=t, j=0 ; i<t+zer_block_size; i++, j++ )										// и добавляем содержимое пакета
			zerkalo[i] = data_buf[j+11+upravl_size] ;									// в конец нашего массива
		part_count-- ;														// уменьшаем на 1 число частей
	}																// теперь в массиве zerkalo всё зеркало целиком
	
	for ( int k = 11+upravl_size+zer_size; k<512; k++)
		printf("%d:%X ", k, data_buf[k]) ;

	memset( data_message.msg_text, '\0', 59 ) ; 	// очистить остатки старого сообщения	

	data_message.msg_flag = data_buf[11+upravl_size+zer_size] ;
	data_message.msg_counter = data_buf[11+upravl_size+zer_size+2] << 8 | data_buf[11+upravl_size+zer_size+1] ;
	data_message.msg_time = data_buf[11+upravl_size+zer_size+6] << 24 | data_buf[11+upravl_size+zer_size+5] << 16 | data_buf[11+upravl_size+zer_size+4] << 8 | data_buf[11+upravl_size+zer_size+3] ;
	data_message.msg_rand = data_buf[11+upravl_size+zer_size+10] << 24 | data_buf[11+upravl_size+zer_size+9] << 16 | data_buf[11+upravl_size+zer_size+8] << 8 | data_buf[11+upravl_size+zer_size+7] ;

	for ( int k = 11+upravl_size+zer_size+12, i = 0; i<59; k++, i++)
    		data_message.msg_text[i] = data_buf[k];

	return 0 ;
}

void catchsig(int sig)

{
	switch (sig) 
	{
		case SIGUSR1: ipx_flag = 0 ; break ;
		case SIGUSR2: ipx_flag = 1 ; break ;
		case SIGTERM: die_flag = 0 ;
	}
}

void set_pid_file(const char* Filename)
{
    FILE* f;

    f = fopen(Filename, "w+");
    if (f)
    {
        fprintf(f, "%u", getpgrp());
        fclose(f);
    }
}

