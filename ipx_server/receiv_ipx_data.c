#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <shmstorage.h>
#include <ipx_server.h>
#include <typedefs.h>

#include <vector>

unsigned char data_buf[BUF_SIZE] ;			// глобальный массив для хранения текущего IPX пакета
extern SHMStorage<ipx_data_data_t> *ipxSharedData ;


int receiv_ipx_data( int upravl_size, int mode, int part_count )				
							
{
	ipx_data_data_t ipx_data_data ;			// структура хранящая принятый пакет с данными

	// получаем данные из разделяемой памяти, записываем их в массив data_buf[BUF_SIZE]
	ipx_data_data = ipxSharedData->get() ;
printf("data_buf[9+upravl_size](num_zerk)=%d\ndata_buf[10+upravl_size](num_block)=%d\n", data_buf[9+upravl_size], data_buf[10+upravl_size]) ;
	if( mode == FIRST_ZER_BLOCK )							// если требуется поискать первый блок в зеркале
	{
		if( ipx_data_data.data_buf[9+upravl_size] != data_buf[9+upravl_size] )	// проверяем, что номер предыдущего зеркала 
		{									// не совпадает с номером текущего
			for(int j=0;j<BUF_SIZE;j++)					// и тогда сохраняем зеркало в массив
				data_buf[j] = ipx_data_data.data_buf[j] ;
			return data_buf[9+upravl_size] ;				// возвращаем номер принятого зеркала в случае, когда сохранили нужный пакет
		}		
		return -1 ;								// или -1 в случае не успеха.
	}

	if( mode == NEXT_ZER_BLOCK )							// если же требуются последующие блоки
	{										// одного и того же зеркала
		if( ipx_data_data.data_buf[9+upravl_size] == data_buf[9+upravl_size] && ipx_data_data.data_buf[10+upravl_size] == part_count )	// проверяем, что номер предыдущего зеркала 
		{									// совпадает с номером текущего
printf("data_buf[9+upravl_size](num_zerk)=%d\ndata_buf[10+upravl_size](num_block)=%d\n", data_buf[9+upravl_size], data_buf[10+upravl_size]) ;
			for(int j=0;j<BUF_SIZE;j++)					// и тогда сохраняем зеркало в массив
				data_buf[j] = ipx_data_data.data_buf[j] ;
			return data_buf[9+upravl_size] ;				// возвращаем номер принятого зеркала в случае, когда сохранили нужный пакет
		}		
		return -1 ;								// или -1 в случае не успеха.		
	}
}
