#ifndef IPX_SERVER_H
#define IPX_SERVER_H

#define PATH_CONFIG "/etc/ask/ask.conf"
#define FIRST_ZER_BLOCK 1
#define NEXT_ZER_BLOCK	0
#define NO_IPX_PACKET 1
#define IPX_PACKET 0

extern int IsDaemon ;										// глобальная переменная объявлена в другом файле (cmd_line.c)
extern unsigned char data_buf[] ;								// глобальный массив для хранения текущего IPX пакета объявлен в другом файле (receiv_ipx_data)
extern unsigned char* zerkalo ;									// динамический массив для хранения зеркала
extern char* post_data ;									// массив для хранения посылки POST
extern struct sockaddr_ipx sipx ;								// глобальная переменная объявлена в другом файле (make_socket.c)
extern struct tm *ptime ;									// указатель на структуру типа tm, для хранения времени
extern time_t lt ;	                        						// сколько секунд прошло от большого взрыва
extern int loc_num ;
extern int l_num ;
extern unsigned char ask_data[150] ;								// массив хранящий текущее значение всех АСК данных (для записи в .ask файл)
extern char file_name[50] ;									// строка с названием текущего .ask файла
extern int id ;
extern struct sigaction sa ;

int print_log( const char *fmt, ... ) ;								// ведущая лог или выводящая на экран если программа не демон
void cmd_line ( int argc, char *argv[] ) ;							// анализ командной строки (ключей программы)
int ipx_server( void ) ;									// основная функция приложения
int make_socket( int *piSocket, int *len_addr ) ;						// функция создания сокета
int receiv_ipx_data( int, int, int ) ;								// функция извлечения ipx пакета из разделяемой памяти

int save_zer( int zer_size, int zer_block_size, int upravl_size ) ;				// функция сохраняет зеркало в массив в динамической памяти
												// (ей передаем размер зеркала, размер постоянных данных в пакете и сколько пакетов ожидать, для сбора зеркала)

void get_post_data(int) ;									// упаковка пакета post_data для последующей отправки
double get_time(void) ;										// функция для получения времени и текущего значения секунд с учетом микросекунд
void get_config(void) ;										// функция читает данные из файла конфигурации
int get_ask_data(unsigned char*) ;								// функция извлекает АСК данные из системы и помещает их в массив
void settimer(int) ;
int gettimer(void) ;
void catchsig(int) ;
int ipx_checker(void) ;
void catch_usr1_usr2(int) ;
void set_pid_file(const char*) ;
float get_board_temp( void ) ;

int get_gps_data(int*, int*, double*, int*, int*, double*, int*) ;				// функция извлекает данные GPS из разделяемой памяти
int get_fuel_data(int*) ;									// функция извлекает данные по топливу из разделяемой памяти
int ipx_daemon(void) ;

#endif // IPX_SERVER_H
