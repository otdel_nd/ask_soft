#ifndef WRITE_ASK_FILE_H
#define WRITE_ASK_FILE_H

int write_ask_file( int zer_size, int *count_record, char *file_name ) ;					// функция должна дописывать текущее зеркало в .ask файл
extern unsigned char* zerkalo ;											// динамический массив для хранения зеркала
extern int loc_num ;
extern struct tm *ptime ;											// указатель на структуру типа tm, для хранения времени

#endif //WRITE_ASK_FILE_H
