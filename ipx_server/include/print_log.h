#ifndef PRINT_LOG_H
#define PRINT_LOG_H

int print_log( const char *fmt, ... ) ;

#endif //PRINT_LOG_H
