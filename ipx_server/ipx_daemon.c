/* эту функцию нужно запускать в дочернем процессе
задача функции - создать сокет (make_socket()), получать пакеты IPX
и сохранять их в буфре в разделяемой памяти, функция должна работать
постоянно и если, пакеты IPX приходить перестали, нужно послать
соответствующий сигнал или передать как-то
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <shmstorage.h>
#include <ipx_server.h>
#include <typedefs.h>

extern pid_t IPXServer, IPXChecker ;							// дискриптор родительского процесса
extern int die_flag ;									// если ноль завершаем процесс
int ipx_data_flag ;									// установлен тогда, когда приходят визитки
int ipx_visi_flag ;									// установлен тогда, когда приходят данные
extern int ipx_flag ;

int ipx_daemon()
{
	IPXChecker = getpid() ;
	pid_t pid = fork() ;
	
	if (pid == 0)										// дочерний процесс
	{
		unsigned char temp[BUF_SIZE] ;							// массив для временного хранения текущего IPX пакета
		SHMStorage<ipx_data_data_t> ipxSharedData("ipx_data") ;				// объект класса SHMStorage для сохранения IPX пакета c данными в разделяемую память
		SHMStorage<ipx_data_visit_t> ipxSharedVisit("ipx_visit") ;			// объект класса SHMStorage для сохранения данных IPX-визитки в разделяемую память
		ipx_data_visit_t ipx_data_visit ;						// структура, которую нужно заполнить и отправить в разделяему память 
		ipx_data_data_t ipx_data_data ;

		int ipxSocket = 0 ;								// дискриптор сокета
		int len_addr ;									// размер структуры, хранящей адрес соедиенения

		if ( make_socket( &ipxSocket, &len_addr ) != 0 )		 		// создаем сокет и настраиваем соединение
		{
			return -1 ;
		}

		// получаем данные через созданный сокет, записываем их в массив data_buf[BUF_SIZE]

		do {
			recvfrom(ipxSocket, temp, BUF_SIZE, 0, (struct sockaddr *)&sipx, (socklen_t*)&len_addr) ;
			if (temp[8] == 0 && temp[0] == 0 && temp[1]+temp[2]+temp[3] == id)		// если приняли пакет-визитку от нужного устройства
			{
				ipx_data_visit.zer_size = temp[42] << 8 | temp[41] ;		// определим размер зеркала
				ipx_data_visit.zer_block_size = temp[44] << 8 | temp[43] ;	// определим размер части зеркала
				ipx_data_visit.upravl_size = temp[40] ;				// определим размер постоянных данных передающихся в каждом пакете (нужно для вычисления смещения зеркала относительно начала пакета)
				ipxSharedVisit.set(ipx_data_visit) ;				// посылаем структуру в разделяемую память
				kill(IPXChecker, SIGUSR1) ;					// отправляем сигнал процессу-родителю, о наличии визиток
			}
			else if (temp[8] == 1 && temp[0] == 0 && temp[1]+temp[2]+temp[3] == id)		// если приняли пакет-данные от нужного устройства
			{
				for(int i=0; i<BUF_SIZE; i++)
					ipx_data_data.data_buf[i] = temp[i] ;			// копируем массив в структуру ipx_data_visit
				ipxSharedData.set(ipx_data_data) ;				// посылаем структуру в разделяемую память
				kill(IPXChecker, SIGUSR2) ;					// отправляем сигнал процессу-родителю, о наличии пакетов с данными
			}
			else									// если приняли посторонний пакет
			{
				continue ;
			}
		} while (die_flag) ;
	printf("\nПроцесс %d сдох\n", getpid() ) ;
	} // дочерний процесс

	if (pid > 0)
	{											// родительский процесс
		if( ipx_checker() == -1 )							// получает получает сигналы о наличии IPX пакетов, а при их длительном их отстутствии выдает свой сигнал
		{
			print_log( "Не удалось запустить ipx_checker" ) ;
			exit(1) ;
		}
	} // родительский процесс

// тут можно указать инструкции для корректного завершения
}


// получает получает сигналы о наличии IPX пакетов, а при их длительном их отстутствии выдает свой сигнал процессу ipx_server
int ipx_checker(void)	
{
	struct sigaction sa ;									// структура для управления сигналами

	sigemptyset(&sa.sa_mask) ;								// очищаем маску сигналов (не текущего процесса, а структуры sa)
	sa.sa_handler = catch_usr1_usr2 ;							// указатель на функцию-обработчик сигналов
	sa.sa_flags = 0|SA_NODEFER ;

	sigaction(SIGUSR1, &sa, NULL) ;								// устанавливает новую диспозицию для сигнала SIGUSR1
	sigaction(SIGUSR2, &sa, NULL) ;								// устанавливает новую диспозицию для сигнала SIGUSR2

	do {
		ipx_visi_flag = 0 ;								// считаем, что IPX пакетов нет
		ipx_data_flag = 0 ;
		sleep(3) ;									// до поступления сигнала (получения IPX пакета)
		if( ipx_visi_flag == 0 && ipx_data_flag == 0 )					// если оба сигнала получены
			kill(IPXServer, SIGUSR1) ;						// сообщаем серверу
		else
			kill(IPXServer, SIGUSR2) ;						// сообщаем серверу, если пакетов до сих пор нет
	} while(die_flag) ;
	printf("\nПроцесс %d сдох\n", getpid() ) ;
	return -1 ;
}

void catch_usr1_usr2(int sig)
{
	switch (sig)
	{
		case SIGUSR1: ipx_visi_flag = 1; break ;
		case SIGUSR2: ipx_data_flag = 1; break ;
	}
}
