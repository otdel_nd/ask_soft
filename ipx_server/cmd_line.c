// анализируем параметры командной строки
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include <print_log.h>
int IsDaemon = 0 ;						// по умолчанию программа - не демон

void cmd_line( int argc, char *argv[] ) 			// argc - количество аргументов командной строки, argv - массив указателей на строки
{
	int k ;

	for ( k = 1 ; k < argc ; k++ )
	{
		if ( strcmp( argv[k], "--help" ) == 0 ) 	// ищем строку --help, если нашли
		{
			printf("Справка\n") ;			// печатаем справку		 
			exit(0) ;				// выходим совсем из программы
		}
	}

	for ( k = 1 ; k < argc ; k++ )
	{
		if ( strcmp( argv[k], "--daemon" ) == 0 ) 	// ищем строку --daemon, если нашли
		{
			IsDaemon = 1 ;				// устанавливаем флаг 1 
		}
	}
	
	for ( k = 1 ; k < argc ; k++ )
	{
		if ( strcmp( argv[k], "--stop" ) == 0 ) 	// ищем строку --stop, если нашли
		{
			FILE* f ;
			pid_t pid_gr ;
    			if ((f = fopen("ipx_server_pid", "r"))==NULL) {
				printf("Сервер не был запущен\n") ;
				exit(1) ;
			}

			fscanf(f, "%d", &pid_gr) ;		// считываем идентефикатор группы процессов IPX-сервера
			fclose(f) ;
			killpg(pid_gr, SIGTERM) ; 		// отправляем сигнал группе процессов 
			unlink("ipx_server_pid") ;		// удаляем файл
			
			if ((f = fopen("ipx_server.log", "at")) == NULL) {
				printf("Не удалось открыть лог-файл\n") ;
				exit(1) ;
			}
			
			fprintf(f, "IPX-сервер остановлен\n") ;	// добавить запись в лог-файл (нужно еще текущее время)
			
			fclose(f) ;			

			exit(0) ;
		}
	}
}
