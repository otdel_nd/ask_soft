#include <string.h>
#include <curl.h>
#include <stdlib.h>

#include <shmstorage.h>
#include <ipx_server.h>
#include <typedefs.h>

extern data_message_t data_message ;
extern SHMStorage<post_data_t>* postSharedData ;
uint16_t prev_msg_counter = 0;

struct										// структура c текущими параметрами локомотива
{										// для отправки на сайт
	char position ;								// Позиция
	int speed ;								// Скорость
	int rpm ;								// Обороты
	int a1 ;								// Сила тяги
	int a2 ;								// Работа в тяге
	float32_t a3 ;								// Pм вх. диз.
	float32_t a4 ;								// P т. вх. ФТОТ
	float32_t a5 ;								// Тв вых. диз.
	float32_t a6 ;								// Тм вых. диз.
	float32_t a7 ;								// U G1 1 зв. выпр.
	float32_t a8 ;								// U G1 2 зв. выпр.
	float32_t a9 ;								// ток ТЭД 1
	float32_t a10 ;								// ток ТЭД 2
	float32_t a11 ;								// ток ТЭД 3
	float32_t a12 ;								// ток ТЭД 4
	float32_t a13 ;								// ток ТЭД 5
	float32_t a14 ;								// ток ТЭД 6
	float32_t a15 ;								// U акб
	float32_t a16 ;								// I зар. акб
	float32_t a17 ;								// P пит. маг.
	
	int fuel ;								// топливо
} loc_data ;

struct
{
	int c1 ;
	int c2 ;
	double c3 ;
	int c4 ;
	int c5 ;
	double c6 ;
	int speed ;
} gps_data ;

uint16_t absolute( uint16_t ) ;								// функция получает абсолютное значение

void get_post_data( int mode )
{
	char post_data[400] ;								// массив для хранения посылки POST
	post_data_t post_data_record ;							// структура из typedefs.h для упаковки POST строки
	char tmp[120] ;									// временный массив

	loc_data.position = zerkalo[237] ;						// заполняем структуру с интересующими нас параметрами локомотива
//printf("\nпозиция индекс 237 = %d\n", loc_data.position) ;
	loc_data.speed = absolute(zerkalo[252] << 8 | zerkalo[251]) ;			// скорость
//printf("скорость индекс 251,252 = %d\n", loc_data.speed ) ;
	loc_data.rpm = absolute(zerkalo[281] << 8 | zerkalo[280]) ;			// частота вращения коленвала дизеля
//printf("частота вращения коленвала дизеля индекс 280,281 = %d\n", loc_data.rpm ) ;	
	loc_data.a1 = absolute(zerkalo[254] << 8 | zerkalo[253]) ;			// сила тяги/тормоза
//printf("сила тяги/тормоза индекс 253,254 = %d\n", loc_data.a1 ) ;
	loc_data.a2 = absolute(zerkalo[239] << 8 | zerkalo[238]) ;			// мощность ЭДТ (в тормозе)/ мощность тепл. (уточнить где в зеркале??)
//printf("мощность ЭДТ (в тормозе)/ мощность тепл. индекс 238,239 = %d\n", loc_data.a2 ) ;
	loc_data.a3 = (absolute(zerkalo[269] << 8 | zerkalo[268]))*0.01 ;		// давление масла на входе дизеля
//printf("давление масла на входе дизеля индекс 268,269 = %f\n", loc_data.a3 ) ;
	loc_data.a4 = (absolute(zerkalo[45] << 8 | zerkalo[44]))*0.0078125 ;		// давление топлива на входе ФТОТ
//printf("давление топлива на входе ФТОТ индекс 44,45 = %f\n", loc_data.a4 ) ;
	loc_data.a5 = (((absolute(zerkalo[81] << 8 | zerkalo[80]))/2048.F)*251)-100 ;	// температура воды на выходе из дизеля
//printf("температура воды на выходе из дизеля индекс 80,81 = %f\n", loc_data.a5 ) ;
	loc_data.a6 = (((absolute(zerkalo[79] << 8 | zerkalo[78]))/2048.F)*251)-100 ;	// температура масла на выходе дизеля
//printf("температура масла на выходе дизеля индекс 78,79 = %f\n", loc_data.a6 ) ;
	loc_data.a7 = (absolute(zerkalo[111] << 8 | zerkalo[110]))*0.48828125 ;		// преобр. измерит. выпрямленное напряжение 1-ой звезды тягового генератора G1 (нужно уточнять номер индекса в зеркале)
//printf("преобр. измерит. выпрямленное напряжение 1-ой звезды тягового генератора G1 индекс 110,111 = %f\n", loc_data.a7 ) ;
	loc_data.a8 = (absolute(zerkalo[97] << 8 | zerkalo[96]))*0.48828125 ;		// преобр. измерит. выпрямленное напряжение 2-ой звезды тягового генератора G1 (нужно уточнять номер индекса в зеркале)
//printf("преобр. измерит. выпрямленное напряжение 2-ой звезды тягового генератора G1 индекс 96,97 = %f\n", loc_data.a8 ) ;
	loc_data.a9 = (absolute(zerkalo[59] << 8 | zerkalo[58]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД1
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД1 индекс 58,59 = %f\n", loc_data.a9 ) ;
	loc_data.a10 = (absolute(zerkalo[61] << 8 | zerkalo[60]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД2
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД2 индекс 60,61 = %f\n", loc_data.a10 ) ;
	loc_data.a11 = (absolute(zerkalo[63] << 8 | zerkalo[62]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД3
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД3 индекс 62,63 = %f\n", loc_data.a11 ) ;
	loc_data.a12 = (absolute(zerkalo[65] << 8 | zerkalo[64]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД4
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД4 индекс 64,65 = %f\n", loc_data.a12 ) ;
	loc_data.a13 = (absolute(zerkalo[67] << 8 | zerkalo[66]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД5
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД5 индекс 66,67 = %f\n", loc_data.a13 ) ;
	loc_data.a14 = (absolute(zerkalo[69] << 8 | zerkalo[68]))*0.48828125 ;		// преобр. измерит. ток в режиме тяги и реостат ТЭД6
//printf("преобр. измерит. ток в режиме тяги и реостат ТЭД6 индекс 68,69 = %f\n", loc_data.a14 ) ;
	loc_data.a15 = (absolute(zerkalo[107] << 8 | zerkalo[106]))*0.0732421875 ;	// преобр. измерит.напряжение аккумуляторной батареи
//printf("преобр. измерит.напряжение аккумуляторной батареи индекс 106,107 = %f\n", loc_data.a15 ) ;
	loc_data.a16 = (absolute(zerkalo[105] << 8 | zerkalo[104]))*0.0732421875 ;	// преобр. измерит.ток заряда батареи
//printf("преобр. измерит.ток заряда батареи индекс 104,105 = %f\n", loc_data.a16 ) ;
	loc_data.a17 = (absolute(zerkalo[75] << 8 | zerkalo[74]))*0.0078125 ;		// давление питательной магистрали
//printf("давление питательной магистрали индекс 74,75 = %f\n", loc_data.a17 ) ;

	memset( post_data, '\0', 400 ) ;

	get_gps_data(&gps_data.c1, &gps_data.c2, &gps_data.c3, &gps_data.c4, &gps_data.c5, &gps_data.c6, &gps_data.speed) ;		// получим GPS-координаты
	get_fuel_data(&loc_data.fuel) ;						// получим топливо

	get_time() ;								// узнаем время
	strcat(post_data, "dt=") ;						// начинаем формировать посылку, пакуем:
	strftime(tmp, 25, "%Y-%m-%d %H:%M:%S", ptime) ;				// время и дату сбора данных
	strcat(post_data, tmp) ;

	strcat(post_data, "&ln=");						// номер локомотива (как в файле конфигурации)

	if(loc_num%10 == 0)
		sprintf(tmp, "%d", loc_num+1);
	else
		sprintf(tmp, "%d", loc_num+1) ;
	strcat(post_data, tmp);

	strcat(post_data, "&fuel=");						
	sprintf(tmp, "%d", loc_data.fuel);
	strcat(post_data, tmp);

	strcat(post_data, "&c1=");						
    	sprintf(tmp, "%d", gps_data.c1);
    	strcat(post_data, tmp);

	strcat(post_data, "&c2=");
    	sprintf(tmp, "%02d", gps_data.c2);
    	strcat(post_data, tmp);

	strcat(post_data, "&c3=");
    	sprintf(tmp, "%f", gps_data.c3);
    	strcat(post_data, tmp);

	strcat(post_data, "&c4=");
    	sprintf(tmp, "%d", gps_data.c4);
    	strcat(post_data, tmp);

    	strcat(post_data, "&c5=");
    	sprintf(tmp, "%02d", gps_data.c5);
    	strcat(post_data, tmp);

    	strcat(post_data, "&c6=");
    	sprintf(tmp, "%f", gps_data.c6);
    	strcat(post_data, tmp);

	if(mode == NO_IPX_PACKET)
	{
		strcat(post_data, "&speed=");						
		sprintf(tmp, "%d", gps_data.speed);
		strcat(post_data, tmp);
	
		for(int i=0 ; i<400 ; i++)						// запакуем post-строку в структуру
			post_data_record.post_data[i] = post_data[i] ;
		postSharedData->set( post_data_record ) ;				// структуру поместим в разделяемую память
		
		return ;
	}

	strcat(post_data, "&speed=");						
	sprintf(tmp, "%d", loc_data.speed);
	strcat(post_data, tmp);

	strcat(post_data, "&position=");					
	sprintf(tmp, "%d", loc_data.position);
	strcat(post_data, tmp);

	strcat(post_data, "&rpm=");						
	sprintf(tmp, "%d", loc_data.rpm);
	strcat(post_data, tmp);

	strcat(post_data, "&a1=");						
	sprintf(tmp, "%d", loc_data.a1);
	strcat(post_data, tmp);

	strcat(post_data, "&a2=");						 
	sprintf(tmp, "%d", loc_data.a2);
	strcat(post_data, tmp);

	strcat(post_data, "&a3=");						 
	sprintf(tmp, "%.2f", loc_data.a3);
	strcat(post_data, tmp);

	strcat(post_data, "&a4=");						 
	sprintf(tmp, "%.2f", loc_data.a4);
	strcat(post_data, tmp);

	strcat(post_data, "&a5=");						 
	sprintf(tmp, "%.2f", loc_data.a5);
	strcat(post_data, tmp);

	strcat(post_data, "&a6=");						 
	sprintf(tmp, "%.2f", loc_data.a6);
	strcat(post_data, tmp);

	strcat(post_data, "&a7=");						
	sprintf(tmp, "%.2f", loc_data.a7);
	strcat(post_data, tmp);

	strcat(post_data, "&a8=");						
	sprintf(tmp, "%.2f", loc_data.a8);
	strcat(post_data, tmp);

	strcat(post_data, "&a9=");						 
	sprintf(tmp, "%.2f", loc_data.a9);
	strcat(post_data, tmp);

	strcat(post_data, "&a10=");						 
	sprintf(tmp, "%.2f", loc_data.a10);
	strcat(post_data, tmp);

	strcat(post_data, "&a11=");						 
	sprintf(tmp, "%.2f", loc_data.a11);
	strcat(post_data, tmp);

	strcat(post_data, "&a12=");						 
	sprintf(tmp, "%.2f", loc_data.a12);
	strcat(post_data, tmp);

	strcat(post_data, "&a13=");						
	sprintf(tmp, "%.2f", loc_data.a13);
	strcat(post_data, tmp);

	strcat(post_data, "&a14=");						
	sprintf(tmp, "%.2f", loc_data.a14);
	strcat(post_data, tmp);

	strcat(post_data, "&a15=");						
	sprintf(tmp, "%.2f", loc_data.a15);
	strcat(post_data, tmp);

	strcat(post_data, "&a16=");						 
	sprintf(tmp, "%.2f", loc_data.a16);
	strcat(post_data, tmp);

	strcat(post_data, "&a17=");						 
	sprintf(tmp, "%.2f", loc_data.a17);
	strcat(post_data, tmp);

	if(data_message.msg_flag && data_message.msg_counter != prev_msg_counter)
	{
		uint16_t str[60] ;
		prev_msg_counter = data_message.msg_counter ;			// сохраним предыдущее число сообщений

		strcat(post_data, "&code1=") ;
	
		for(int i=0 ; i<59 ; i++)
		{
			str[i] = data_message.msg_text[i] ;
			if(str[i] >= 0x80 && str[i] <= 0x9F)			// если в str[i] буква от А до Я (кроме Ё)
				str[i]+=0xD010 ;				// переведем в юникод (UTF-8)
			else if(str[i] >= 0xA0 && str[i] <= 0xAF)		// если в str[i] буква от а до п (кроме ё)
				str[i]+=0xD010 ;				// переведем в юникод (UTF-8)
			else if(str[i] >= 0xE0 && str[i] <= 0xEF)		// если в str[i] буква от р до я
				str[i]+=0xD0A0 ;				// переведем в юникод (UTF-8)
			else
				continue ;
		}

		printf("\nmsg_time=%d\nmsg_counter=%d\nmsg_rand=%d\n", data_message.msg_time, data_message.msg_counter, data_message.msg_rand ) ;

		memset( tmp, '\0', 120 ) ;
		for(int i = 0, j=0 ; i<60 ; i++, j++)				// упорядочиваем сообщение для отправки в post_data
		{
			if(str[i]>0xFF)						// если буква кириллица
			{
				tmp[j] = (uint8_t)(str[i]>>8) ;			
				tmp[j+1] = (uint8_t)((str[i]<<8)>>8) ;
				j++ ;
			}
			else							// если другой символ
				tmp[j] = str[i] ;
		}
		 
		strcat( post_data, tmp ) ;					// присоединим к post_data
	}

	for(int i=0 ; i<400 ; i++)						// запакуем post-строку в структуру
		post_data_record.post_data[i] = post_data[i] ;
	postSharedData->set( post_data_record ) ;				// структуру поместим в разделяемую память
}

uint16_t absolute( uint16_t x ) 
{
	if((x&(0x8000)) != 0){		// если число отрицательное
		return ~x+1 ;		// возвращаем по модулю
	}
	return x ;			// если нет - возвращаем как есть
}
