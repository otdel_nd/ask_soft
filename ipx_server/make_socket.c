#include <sys/socket.h>
#include <sys/types.h>
#include <linux/ipx.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <print_log.h>

#define UPS_TCP_PORT 666
struct sockaddr_ipx sipx ;							// структура для хранения адреса

// функция создания сокета

int make_socket( int *ipxSocket, int *len_addr )
{
	int sk ;								// дискриптор сокета

	sk = socket(AF_IPX, SOCK_DGRAM, AF_IPX) ;				// создаем сокет IPX, тип пакета датаграммы

	if(sk < 0)
	{
		print_log("Ошибка при создании сокета IPX: %d", errno) ;
		return -1 ;
	}

	memset( &sipx, 0, sizeof(sipx) ) ;					// обнуляем структуру и заполняем структуру:
	sipx.sipx_family = AF_IPX ;						// тип сокета - IPX
	sipx.sipx_network = 0L ;						// сеть
	memset( sipx.sipx_node,0,IPX_NODE_LEN ) ;				// очищаем массив хранящий mac-адрес (заполняем его нулями)
	sipx.sipx_node[0] = 0xFF ;
	sipx.sipx_node[1] = 0xFF ;
	sipx.sipx_node[2] = 0xFF ;
	sipx.sipx_node[3] = 0xFF ;
	sipx.sipx_node[4] = 0xFF ;
	sipx.sipx_node[5] = 0xFF ;
	sipx.sipx_port = htons( UPS_TCP_PORT ) ;				// порт
	sipx.sipx_type = 4 ;							// какой-то тип
	*len_addr = sizeof(sipx) ;

	if( bind( sk,(struct sockaddr *)&sipx,sizeof(sipx)) < 0 )		// выполняем привязку сокета к физическому адресу сетевого интерфейса (описанного в структуре sipx)
	{
		print_log("Ошибка при попытке создать соединение: %d", errno) ;
		close ( sk ) ;
		return -1 ;
	}

	*ipxSocket = sk ;							// возвращаем значение дискриптора сокета в вызывающую функцию по адресу переменной ipxSocket
	return 0 ;								// функция возвращает ноль, в случае успеха всего выше выполненного
}
