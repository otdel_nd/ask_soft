#include <time.h>
#include <stdlib.h>

#include <ipx_server.h>

extern "C" {
#include <sys/time.h>
}

struct tm *ptime;							// указатель на структуру типа tm, для хранения времени
time_t lt;	                        				// сколько секунд прошло от большого взрыва

double get_time()
{
	// для таймера
	double t;							// возвращаемое значение в секундах (с учетом микросекунд)
	struct timeval tv;						// структура для хранения времени
	gettimeofday(&tv, NULL);					// получаем системное время
	t = tv.tv_sec + ((double)tv.tv_usec)/1e6;			// время в секундах (с учетом микросекунд)

	// для формирования посылки
        lt = time(NULL);                    				// сколько секунд прошло от большого взрыва
        ptime = localtime(&lt);                 			// заполняем структуру

	return t;                                                       // возвратим время в секундах
}


void settimer(int sec)
{
	struct	itimerval itv ;

	itv.it_value.tv_sec = 0 ;
	itv.it_value.tv_usec = sec ;
	itv.it_interval = itv.it_value ;

	setitimer(ITIMER_REAL, &itv, NULL) ;
}

int gettimer(void)
{
	struct itimerval itv ;
	getitimer( ITIMER_REAL, &itv ) ;

	return itv.it_value.tv_sec ;
}
