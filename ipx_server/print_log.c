#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <ipx_server.h>				// объявления собственных функций в одном файле
#include <print_log.h>				// объявление функции print_log()

// ведение лога в файл или в поток вывода

int print_log( const char *fmt, ... )		// функции передаем строку которую хотим записать в файл (ведет себя как printf)
{
  int R = -1 ;					// но данные записывает в файл mylog.log
  FILE *trg ;					// указатель на файл лога
  char Buffer[16*1024] ;			// буфер записи размером 16кб
  va_list L ;					// используем макросы файла stdarg.h
  va_start(L, fmt) ;				// чтобы наша функция mylog() работала как стандартная printf()
  vsnprintf( Buffer, sizeof(Buffer), fmt, L ) ;	// записываем строку на которую указываeт указатель fmt в наш буфер
  va_end(L) ;

  trg = IsDaemon ? fopen( "ipx_server.log", "at" ) : stderr ; // пишем в лог если демон ( если нет в стандартный поток)
  if ( trg ) 					// если файл открыли (или stderr)
  {
    R = fprintf( trg, "%s", Buffer ) ;	// записываем в файл содержимое буфера
    if ( IsDaemon ) fclose(trg) ;
  }

  return R ; 					// возвращаем количество записанных байт (или -1 в случае ошибки)

}
